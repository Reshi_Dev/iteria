<?php
    session_start();
    if(!isset($_SESSION['currentuser'])){
        header("Location: ../ITERIA/login.php?loginagain");
        exit();
    }
    if($_SESSION['currentuser'] != 12){
        header("Location: ../ITERIA/home.php?permissiondenied");
        exit();
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@700&display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer"
    />
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Sarala&display=swap" rel="stylesheet">

    <title>Document</title>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@700&family=Sarala&display=swap');
        * {
            box-sizing: border-box;
            margin: 0;
            padding: 0;
            font-family: 'Poppins', sans-serif;
        }
        
        #main::-webkit-scrollbar {
            display: none;
        }
        
        .side-bar-text {
            font-size: .9rem;
        }
        /* for sliding btn */

        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }
        
        .switch input {
            opacity: 0;
            width: 0;
            height: 0;
        }
        
        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }
        
        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }
        
        input:checked+.slider {
            background-color: #F6821F;
        }
        
        input:checked+.slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }
        
        .slider.round {
            border-radius: 34px;
        }
        
        .slider.round:before {
            border-radius: 50%;
        }


        
        @media (min-width: 850px) and (max-width: 1100px) {
            .feedback-text {
                font-size: 14px !important;
            }
            .nav-header {
                font-size: 19px !important;
            }
        }
        
        @media (max-width: 850px) {
            .feedback-text {
                font-size: 11px !important;
            }
            .nav-header {
                font-size: 17px !important;
            }
        }
    </style>
</head>

<body class="">
    <main class="w-100" style="height: 50px;">
        <!-- main parent -->
        <div class="d-flex flex-row w-100" style="height:100%;">
            <!-- left part -->
            <?php
                include_once 'component.ad.php';
                draw_side_bar();
                
            ?>
            <!-- right part -->
            <section class="righta w-100">
            <?php
                draw_nav("DASHBOARD");
            ?>
                <section class=" main-content border-dark position-absolute  " id="main" style=" padding: 60px;height:100vh;overflow:scroll; height: 533px;">
                    <!-- your code here! -->
                    <div class="top-cont d-flex justify-content-between">
                        <div class="left container m-2 p-4 w-50" style="background-color: #EEEEEE; border-radius: 20px;">
                            <h3 class="text-center pb-2 fs-2" style="color: #F6821F;">ITERIA</h3>
                            <hr>
                            <!-- Rounded switch -->
                            <form action="" method="post">
                            <div class="container p-4 w-100 d-flex justify-content-between">
                                <h4 class="" >Active Status</h4>
                                <label class="switch">
                                <input type="checkbox" name="activestatus" id="myCheck" <?php 
                                echo !isset($_SESSION['activestatus']) ? "" : (($_SESSION['activestatus'] == "true") ? "Checked" : ""); ?>>
                                <span class="slider round"></span>
                                </label>
                            </div>
                            </form>
                            
                            
                            <hr>
                            <form action="updatesummary.php" method="post">
                                <input class="form-control w-100 my-4" name="r" type="text" placeholder="Offline Revenue">
                                <input class="form-control w-100" name="e" type="text" placeholder="Your Expenses">
                                <button class="btn w-100 my-4 text-white fs-5" type="submit" name="submit" style="background-color: #F6821F;">Update</button>
                            </form>
                            
                        </div>
                        <?php
                            include_once 'connection.php';
                            if(isset($_GET['resetvalues'])){
                                $final = "update admin set grossprofit = 0, revenue = 0, expenses = 0;";
                                mysqli_query($conn, $final);
                            }


                            $admin_sql = "select * from admin;";
                            $r = $conn->query($admin_sql);
                            $data = $r->fetch_assoc();
                        ?>
                        <div class="right container m-2 p-4 w-50" style="background-color: #EEEEEE; border-radius: 20px;">
                            <h3 class="text-center pb-2 fs-2" style="color: #F6821F;">DAILY SUMMARY</h3>
                            <hr>

                            <div class="top row  pt-2">
                                <div class="col m-3 p-3" style="background-color: #F6821F; border-radius: 20px;">
                                    <h5 class="d-inline-block pe-5">Revenue</h5>
                                    <span class="fs-5 text-white">BTN<span class="fs-4 ps-2 text-white"><?php echo $data['revenue'];?></span></span>
                                </div>
                                <div class="col m-3 p-3" style="background-color: #F6821F; border-radius: 20px;">
                                    <h5 class="d-inline-block pe-5 ">Expenses</h5>
                                    <span class="fs-5 text-white ">BTN   <span class="fs-4 ps-2 text-white "><?php echo $data['expenses'];?></span></span>
                                </div>

                            </div>
                            <div class="row ">
                                <div class="col m-3 p-0 " style="background-color: #F6821F; border-radius: 20px; ">
                                    <h4 class="text-center fs-2 mt-3 ">Gross Profit</h4>
                                    <p class="fs-3 text-white text-center w-100 ">BTN <span class="fs-2 ps-2 text-white "><?php echo $data['grossprofit'];?></span></p>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- graph -->
                    <div class="bottom w-100 d-flex justify-content-center mt-4 ">
                        <div class=" w-100 p-4 " style="background-color:#EEEEEE; border-radius: 20px; ">
                            <h3 class="text-center pb-2 " style="color: #F6821F; ">YOUR SALES</h3>
                            <hr>
                            <canvas id="myChart" width="400" height="170"></canvas>
                        </div>
                        
                    </div>
                    <div class="d-flex">
                    <div class="d-inline-block w-100"><form action="updatedailysales.php" method="post">
                        <button class="btn w-50 my-4 text-white fs-5" type="submit" name="submitweekly" style="background-color: #F6821F;">Update</button>
                        </form></div>
                        <div class="d-inline-block w-100"><form  action="resetsales.php" method="post">
                        <button class="btn w-50 my-4 text-white fs-5" type="submit" name="resetsales" style="background-color: #F6821F;">Reset</button>
                        </form></div>
                    </div>
                        
                        
                    ​<?php
                        $z = "select * from sales";
                        $r = $conn->query($z);
                        $weelky_sales = array();
                        while ($row = mysqli_fetch_assoc($r)){
                            array_push($weelky_sales, $row['profit']);
                        }
                        
                    ?>
                </section>
                ​
            </section>
        </div>
        ​
    </main>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script >
            const labels = ['day 1', 'day 2', 'day 3', 'day 4', 'day 5', 'day 6', 'day 7', 'day 8', 'day 9', 'day 10', 'day 11', 'day 12', 'day 13', 'day 14', 'day 15' ];
            
            const data = {
                labels: labels,
                datasets: [{
                    label: 'Sales',
                    backgroundColor: '#F6821F',
                    borderColor: '#F6821F',
                    // data: [0, 1, 5, 2, 20, 30, 45,],
                    data: <?php
                       echo  json_encode($weelky_sales);
                    ?>,
                }]
            };
            const config = {
                type: 'line',
                data: data
            };

            var myChart = new Chart(document.getElementById('myChart').getContext('2d'), config);

    </script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script>
        $('#myCheck').click(function() {
            $.ajax({
                    url : 'as.php',
                    method : 'POST',
                    data : {
                        "active_status" : $('#myCheck').prop("checked")
                    }
            })
        })
    </script>
</body>
​

</html>
