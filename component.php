<?php
    require_once 'connection.php';

    function get_total_count(){

        global $conn;
        $result = $conn->query("select * from items");
        $n = mysqli_num_rows($result);
        return $n;
    }
    function draw_components($name, $price, $img, $id){
        $staus = false;
        if(isset($_SESSION['cart'])){
            foreach ($_SESSION['cart'] as $item){
               if($item['itemID'] == $id){
                   $staus = true;
               }
            }
       }else{
           $staus = false;
       }


        $elementNotAdded = "
        <div class=\"card shadow p-2 m-2 d-inline-block\" style=\"width: 14rem; border-radius: 20px;\">
        <form action = \"addtocart.php\" method = \"post\">
        <div class=\"d-block\" width=\"222\" height=\"172\"></div>
        <img src=\"$img\" class=\"card-img-top\" width=\"222\" height=\"142\">
            <div class=\"card-body\">
                <h5 class=\"card-title\">$name</h5>
                <p class=\"card-text\" style=\"color: #F6821F;\">BTN <span id=\"item-price\">$price</span></p>
                <form action\"addtocart.php\" method = \"post\">
                <button type=\"submit\" name = \"add\" class=\"btn card-btn w-100 mt-1 text-white\" style=\"border-radius:15px; background-color: #F6821F;\"> <i class=\"fas fa-shopping-cart d-none d-md-none d-lg-inline me-2\"></i> ADD TO CART</button>
                <input type= \"hidden\" name=\"itemID\" value =\"$id\" >
            </div>
            </form>
        </div>";
        $elementAdded = "
        <div class=\"card shadow p-2 m-2 d-inline-block\" style=\"width: 14rem; border-radius: 20px;\">
        <form action = \"addtocart.php\" method = \"post\">
        <div class=\"d-block\" width=\"222\" height=\"172\"></div>
        <img src=\"$img\" class=\"card-img-top\" width=\"222\" height=\"142\">
            <div class=\"card-body\">
                <h5 class=\"card-title\">$name</h5>
                <p class=\"card-text  text-success\" >BTN <span id=\"item-price\">$price</span></p>
                <form action\"addtocart.php\" method = \"post\">
                <button type=\"submit\" name = \"add\" class=\"btn btn-success card-btn w-100 mt-1 text-white\" style=\"border-radius:15px; \">ADDED TO CART </button>
                <input type= \"hidden\" name=\"itemID\" value =\"$id\" >
            </div>
            </form>
        </div>";

        if($staus){
            echo $elementAdded;
        }else{
            echo $elementNotAdded;
        }


    }
    function draw_cart_components($itemname, $itemprice, $itemimg, $itemid, $qty){
            $value = round($qty/$itemprice);
            if($value == 0){
                $value = 1;
            }
           
            $element2 = "
           
            <div class=\"row mb-4 d-flex justify-content-between align-items-center\">
            <div class=\"col-md-2 col-lg-2 col-xl-2\">
                <img src=\"$itemimg\" class=\"img-fluid rounded-3\" alt=\"s\">
            </div>
            <div class=\"col-md-3 col-lg-3 col-xl-3\">
                <h6 class=\"text-black mb-0\">$itemname</h6>
            </div>
            <div class=\"col-md-3 col-lg-3 col-xl-2 d-flex\">
            <form action=\"cart.php?qtyid=$itemid\" method=\"post\" class=\"d-flex\">


                <input   type=\"number\" name=\"qty\" value=\"$value\" class=\"form-control form-control-sm\" />
                <input type=\"submit\" value=\"ok\" class=\"form-control form-control-sm\"/>

            </form>
            </div>
            <div class=\"col-md-3 col-lg-2 col-xl-2 offset-lg-1\">
                <h6 class=\"mb-0\">BTN $itemprice</h6>
            </div>
            
            <div class=\"col-md-1 col-lg-1 col-xl-1 text-end\">
            <form action=\"cart.php?action=remove&id=$itemid\" method=\"post\">
                <button type=\"submit\" name=\"remove\" class=\"btn text-muted\"><i class=\"fas fa-times\" style=\"color:#F6821F;\"></i></button >
            </form>
            </div>

            
        </div>
   

        <hr class=\"my-4\">
            ";
            echo $element2;
    }
    function draw_prev_btn(){
        $content = "
        <form action=\"menu.php\" method=\"post\">
        <div class=\"navigate-btn btn\"> <button id=\"#nextt-btn\" type=\"submit\" name =\"prev\" class=\"btn rounded-pill px-3 ms-3 text-white\" style=\"background-color:#F6821F; \" >PREV</button></div>
        </form>
        ";
        echo $content;
    }
    function draw_next_btn(){
        $content = "
        <form action=\"menu.php\" method=\"post\">
        <div class=\"navigate-btn btn\"> <button id=\"#nextt-btn\" type=\"submit\" name =\"next\" class=\"btn rounded-pill px-3 mx-2 text-white\" style=\"background-color:#F6821F; \" >NEXT</button></div>
        </form>
        ";
        echo $content;
    }
    function available_card(){
        $sql = "SELECT * FROM items where itemstatus = 1 LIMIT 4;";
        $result = $conn->query($sql);
        if(mysqli_num_rows($result) > 0){    
            while ($row = mysqli_fetch_assoc($result)){
                // $img = $row['itemimg'];
                // $title = $row['itemname'];
                // $pri =$row['itemprice'];
                echo "<div class=\"card shadow p-2 mx-3 d-inline-block \" style=\"width: 14rem; border-radius: 20px;\">
                <img src=\"$img\" class=\"card-img-top \" alt=\"...\" width=\"222\" height=\"142\">
                <div class=\"card-body\">
                    <h5 class=\"card-title\">$title</h5>
                    <p class=\"card-text\" style=\"color: #F6821F;\">BTN <span id=\"item-price\">$pri</span></p>
                    <a href=\"menu.php\" class=\"btn w-100 mt-1 text-white\" style=\"border-radius:15px; background-color: #F6821F;\"> <i class=\"fas fa-shopping-cart me-2\"></i> ADD TO CART</a>
                </div>
            </div>";
            }
        }else{
            echo  '<div class = "text-dark fs-5 ps-4 text-normal" > <br> Sorry there is nothing to eat!!! :((( <div/>';
        }
    }
?>

<?php
    
?>