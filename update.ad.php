<?php
    session_start();
    if(!isset($_SESSION['currentuser'])){
        header("Location: ../ITERIA/login.php?loginagain");
        exit();
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@700&display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer"
    />
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Sarala&display=swap" rel="stylesheet">

    <title>Document</title>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@700&family=Sarala&display=swap');
        * {
            box-sizing: border-box;
            margin: 0;
            padding: 0;
            font-family: 'Poppins', sans-serif;
        }
        
        #main::-webkit-scrollbar {
            display: none;
        }
        
        .side-bar-text {
            font-size: .9rem;
        }
        /* for sliding btn */

        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }
        
        .switch input {
            opacity: 0;
            width: 0;
            height: 0;
        }
        
        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }
        
        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }
        
        input:checked+.slider {
            background-color: #F6821F;
        }
        
        input:checked+.slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }
        
        .slider.round {
            border-radius: 34px;
        }
        
        .slider.round:before {
            border-radius: 50%;
        }
        td {
            padding: 20px;
        }

        
        @media (min-width: 850px) and (max-width: 1100px) {
            .feedback-text {
                font-size: 14px !important;
            }
            .nav-header {
                font-size: 19px !important;
            }
        }
        
        @media (max-width: 850px) {
            .feedback-text {
                font-size: 11px !important;
            }
            .nav-header {
                font-size: 17px !important;
            }
        }
    </style>
</head>

<body class="">
    <main class="w-100" style="height: 50px;">
        <!-- main parent -->
        <div class="d-flex flex-row w-100" style="height:100%;">
            <!-- left part -->
            <?php
                include_once 'connection.php';
                include_once 'component.ad.php';
                draw_side_bar();
            ?>
            <!-- right part -->
            <section class="righta w-100">
            <?php
                draw_nav("UPDATE");
            ?>
                <!-- position-absolute bottom-0  -->
                <section class=" main-content border-dark " id="main" style=" padding: 60px; height:100vh;overflow:scroll; height: 533px;">
                    <!-- your code here! -->
                    <?php
                        include_once 'connection.php';
                        if(isset($_GET['action'])){
                            if($_GET['action'] == 'remove'){
                                $id = $_GET['id'];
                                $q = "UPDATE items set itemstatus = 0 where itemid = $id;";
                                mysqli_query($conn, $q);
                                
                            }
                        }
                    
                    ?>
                    <div class="row">
                        <div class="col-lg-6">
                            <h3 class="text-center w-100">Today's Menu</h3>
                            <div class="container p-5" style="background-color: #F0F0F0;">
                                <div class="row">
                                    <form action="updatemenu.php" method="post">
                                        <div class="d-flex">
                                            <div class="col-8">
                                            
                                                
                                            <select class="select" name="item">
                                                <?php
                                                    $sql = "select * from items where itemstatus = 0;";
                                                    $result = $conn->query($sql);
                                                    while ($row = mysqli_fetch_assoc($result)){
                                                        $itemname = $row['itemname'];
                                                        $ele = "<option value=\"$itemname\">$itemname</option>";
                                                        echo $ele;
                                                    }
                                                ?>
                                            </select>

                                                
                                            </div>


                                            <div class="col-4">
                                                <a href="#"><button class="btn btn-warning container-fluid text-white" type="submit" name="submit" style="background-color: #F6821F;">Add</button></a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                
                                <div class="container mt-5" style="background-color: #F6821F;">
                                    <table class="w-100 text-white">
                                        <?php
                                             $sql = "select * from items where itemstatus = 1;";
                                             $result = $conn->query($sql);
                                             while ($row = mysqli_fetch_assoc($result)){
                                                 $itemname = $row['itemname'];
                                                 $itemid = $row['itemid'];
                                                 $ele = "<tr>
                                                             <td class = \"text-center\">$itemname</td>
                                                             <td class = \"text-center\"><form action=\"update.ad.php?action=remove&id=$itemid\" method=\"post\">
                                                             <button type=\"submit\" name=\"remove\" class=\"btn text-muted\"><i class=\"fas fa-times\" style=\"color:white;\"></i></button >
                                                            </form></td>
                                                        </tr>";
                                                 echo $ele;
                                             }
                                        ?>
                                        <form action="">

                                        </form>
                                    </table>
                                </div>
                            </div>

                        </div>
                        
                        <div class="col-lg-6" >
                            <?php
                                if(isset($_GET['stockupdatesuccess']))
                            ?>
                            <h3 class="text-center w-100">Stock Update</h3>
                            
                            <div class="container py-5 " style="background-color: #F0F0F0;">
                                
                                <?php
                                if(isset($_GET['stockupdatesuccess'])){
                                    echo "<span class=\"position-relative text-success \"> stock updated!</span>";
                                }else if(isset($_GET['selectonecategory'])){
                                    echo "<span class=\"position-relative text-danger \"> Please select a category!</span>";

                                }
                                ?>
                                <form action="stockupdate.php" method="post" enctype="multipart/form-data">

                                <div class="mb-3">
                                    <input type="text" name="itemname" class="form-control"  placeholder="Item Name">
                                </div>
                                <div class="mb-3">
                                    <input type="text" name="itemprice" class="form-control"  placeholder="Price">
                                </div>
                                <div class="mb-3">
                                Select all possible category <br>
                                <input type="checkbox" name="cate[]" value="lunchdinner" />Lunch/Dinner<br>
                                <input type="checkbox" name="cate[]" value="drinks" />Drinks<br />
                                <input type="checkbox" name="cate[]" value="groceries" />Groceries<br />
                                <input type="checkbox" name="cate[]" value="veg" />Veg<br />
                                <input type="checkbox" name="cate[]" value="nonveg" />Non-veg
                                
                                </div>
                                <div class=" mb-3" style="height: 3rem;">
                                    <input type="file" name="itemimage" class="form-control">
                                </div>
                                <div>
                                    <button type="submit"name="submit" class="btn btn-warning btn-lg btn-block w-100 text-white" style="background-color: #F6821F;">UPDATE</button>
                                </div>

                                </form>
                            </div>
                        </div>
                        
                    </div>


                    </div>


                    ​
                </section>
                ​
            </section>
        </div>
        ​
    </main>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script src="graph.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    ​<script>
                function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
            $('#blah').attr('src', e.target.result).width(150).height(190);
            };

            reader.readAsDataURL(input.files[0]);
        }
        }
    </script>
</body>
​

</html>
