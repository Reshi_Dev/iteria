<?php
    include_once 'connection.php';

    function draw_nav($title){
        $nav = "<nav class=\"navbar navbar-expand-lg d-flex flex-row navbar-light bg-light shadow \" style=\"height: 70px;\">
        <div class=\"d-flex w-100 justify-content-between\">
            <div class=\"container-fluid text-center\">
                
                <span class=\"navbar-brand text-center w-100 fw-5 py-3 nav-header\">$title</span>
            </div>
        </div>
        </nav>
        </nav>";
        echo $nav;
    }
    function draw_side_bar(){
        $side_Bar = "<section class=\"left col-2 d-flex flex-column justify-content-start \" style=\"background-color: #F6821F; height: 100vh;\">
        <div class=\"logo mt-3\">
        <svg id=\"Layer_2\" data-name=\"Layer 2\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 384.96 105.45\"  class=\"d-none d-sm-none d-md-block rounded mx-auto d-block img-fluid\" width=\"200\" height=\"40\">
        <defs>
            <style>
            .cls-1 {
                fill: #fff;
            }

            .cls-2 {
                fill: #ff931e;
                stroke: #fff;
                stroke-miterlimit: 10;
            }
            </style>
        </defs>
        <g id=\"Layer_1-2\" data-name=\"Layer 1\">
            <g>
            <g>
                <path class=\"cls-1\" d=\"m.56,99.59c1,.61,2.46,1.13,3.99,1.13,2.28,0,3.61-1.2,3.61-2.94,0-1.61-.92-2.53-3.25-3.43-2.82-1-4.56-2.46-4.56-4.89,0-2.69,2.23-4.68,5.58-4.68,1.77,0,3.05.41,3.81.84l-.61,1.82c-.56-.31-1.72-.82-3.28-.82-2.35,0-3.25,1.41-3.25,2.58,0,1.61,1.05,2.41,3.43,3.33,2.92,1.13,4.4,2.53,4.4,5.07,0,2.66-1.97,4.96-6.04,4.96-1.66,0-3.48-.49-4.4-1.1l.56-1.87Z\"/>
                <path class=\"cls-1\" d=\"m16.12,85.06h2.23v8.32h.08c.46-.67.92-1.28,1.36-1.84l5.27-6.47h2.76l-6.24,7.32,6.73,9.93h-2.64l-5.68-8.47-1.64,1.89v6.58h-2.23v-17.25Z\"/>
                <path class=\"cls-1\" d=\"m34.84,85.06v17.25h-2.23v-17.25h2.23Z\"/>
                <path class=\"cls-1\" d=\"m41.37,85.26c1.07-.18,2.48-.33,4.27-.33,2.2,0,3.81.51,4.84,1.43.95.82,1.51,2.07,1.51,3.61s-.46,2.79-1.33,3.68c-1.18,1.25-3.1,1.89-5.27,1.89-.67,0-1.28-.02-1.79-.15v6.91h-2.23v-17.04Zm2.23,8.32c.49.13,1.1.18,1.84.18,2.69,0,4.32-1.3,4.32-3.68s-1.61-3.38-4.07-3.38c-.97,0-1.71.08-2.1.18v6.7Z\"/>
                <path class=\"cls-1\" d=\"m67.9,86.95h-5.25v-1.89h12.77v1.89h-5.27v15.35h-2.25v-15.35Z\"/>
                <path class=\"cls-1\" d=\"m82.2,85.06v7.22h8.34v-7.22h2.25v17.25h-2.25v-8.09h-8.34v8.09h-2.23v-17.25h2.23Z\"/>
                <path class=\"cls-1\" d=\"m108.21,94.22h-6.7v6.22h7.47v1.87h-9.7v-17.25h9.31v1.87h-7.09v5.45h6.7v1.84Z\"/>
                <path class=\"cls-1\" d=\"m122.56,85.06h2.23v15.38h7.37v1.87h-9.59v-17.25Z\"/>
                <path class=\"cls-1\" d=\"m139.5,85.06v17.25h-2.23v-17.25h2.23Z\"/>
                <path class=\"cls-1\" d=\"m146.02,102.3v-17.25h2.43l5.53,8.73c1.28,2.02,2.28,3.84,3.1,5.6l.05-.03c-.2-2.3-.26-4.4-.26-7.09v-7.22h2.1v17.25h-2.25l-5.48-8.75c-1.2-1.92-2.35-3.89-3.22-5.76l-.08.03c.13,2.17.18,4.25.18,7.11v7.37h-2.1Z\"/>
                <path class=\"cls-1\" d=\"m174.42,94.22h-6.7v6.22h7.47v1.87h-9.7v-17.25h9.31v1.87h-7.09v5.45h6.7v1.84Z\"/>
                <path class=\"cls-1\" d=\"m179.07,105.45c.56-1.51,1.25-4.25,1.54-6.12l2.51-.26c-.59,2.17-1.71,5.02-2.43,6.22l-1.61.15Z\"/>
                <path class=\"cls-1\ d=\"m211.33,93.5c0,5.94-3.61,9.08-8.01,9.08s-7.75-3.53-7.75-8.75c0-5.48,3.4-9.06,8.01-9.06s7.75,3.61,7.75,8.73Zm-13.38.28c0,3.68,2,6.99,5.5,6.99s5.53-3.25,5.53-7.17c0-3.43-1.79-7.01-5.5-7.01s-5.53,3.4-5.53,7.19Z\"/>
                <path class=\"cls-1\" d=\"m216.86,85.29c1.13-.23,2.74-.36,4.27-.36,2.38,0,3.91.43,4.99,1.41.87.77,1.36,1.94,1.36,3.28,0,2.28-1.43,3.79-3.25,4.4v.08c1.33.46,2.12,1.69,2.53,3.48.56,2.41.97,4.07,1.33,4.73h-2.3c-.28-.49-.66-1.97-1.15-4.12-.51-2.38-1.43-3.28-3.45-3.35h-2.1v7.47h-2.23v-17.02Zm2.23,7.86h2.28c2.38,0,3.89-1.31,3.89-3.28,0-2.23-1.61-3.2-3.97-3.22-1.07,0-1.84.1-2.2.2v6.3Z\"/>
                <path class=\"cls-1\" d=\"m233.26,85.29c1.36-.2,2.97-.36,4.73-.36,3.2,0,5.48.74,6.99,2.15,1.54,1.41,2.43,3.4,2.43,6.19s-.87,5.12-2.48,6.7c-1.61,1.61-4.27,2.48-7.62,2.48-1.59,0-2.92-.08-4.04-.2v-16.96Zm2.23,15.25c.56.1,1.38.13,2.25.13,4.76,0,7.34-2.66,7.34-7.32.03-4.07-2.28-6.65-6.99-6.65-1.15,0-2.02.1-2.61.23v13.61Z\"/>
                <path class=\"cls-1\" d=\"m261.86,94.22h-6.7v6.22h7.47v1.87h-9.7v-17.25h9.31v1.87h-7.09v5.45h6.7v1.84Z\"/>
                <path class=\"cls-1\" d=\"m268.15,85.29c1.13-.23,2.74-.36,4.27-.36,2.38,0,3.92.43,4.99,1.41.87.77,1.36,1.94,1.36,3.28,0,2.28-1.43,3.79-3.25,4.4v.08c1.33.46,2.12,1.69,2.53,3.48.56,2.41.97,4.07,1.33,4.73h-2.3c-.28-.49-.67-1.97-1.15-4.12-.51-2.38-1.43-3.28-3.46-3.35h-2.1v7.47h-2.23v-17.02Zm2.23,7.86h2.28c2.38,0,3.89-1.31,3.89-3.28,0-2.23-1.61-3.2-3.97-3.22-1.07,0-1.84.1-2.2.2v6.3Z\"/>
                <path class=\"cls-1\" d=\"m307.34,93.5c0,5.94-3.61,9.08-8.01,9.08s-7.75-3.53-7.75-8.75c0-5.48,3.4-9.06,8.01-9.06s7.75,3.61,7.75,8.73Zm-13.38.28c0,3.68,2,6.99,5.5,6.99s5.53-3.25,5.53-7.17c0-3.43-1.79-7.01-5.5-7.01s-5.53,3.4-5.53,7.19Z\"/>
                <path class=\"cls-1\" d=\"m312.87,102.3v-17.25h2.43l5.53,8.73c1.28,2.02,2.28,3.84,3.1,5.6l.05-.03c-.2-2.3-.26-4.4-.26-7.09v-7.22h2.1v17.25h-2.25l-5.47-8.75c-1.2-1.92-2.35-3.89-3.22-5.76l-.08.03c.13,2.17.18,4.25.18,7.11v7.37h-2.1Z\"/>
                <path class=\"cls-1\" d=\"m332.33,85.06h2.23v15.38h7.37v1.87h-9.59v-17.25Z\"/>
                <path class=\"cls-1\" d=\"m349.27,85.06v17.25h-2.23v-17.25h2.23Z\"/>
                <path class=\"cls-1\" d=\"m355.79,102.3v-17.25h2.43l5.53,8.73c1.28,2.02,2.28,3.84,3.1,5.6l.05-.03c-.2-2.3-.26-4.4-.26-7.09v-7.22h2.1v17.25h-2.25l-5.47-8.75c-1.2-1.92-2.35-3.89-3.22-5.76l-.08.03c.13,2.17.18,4.25.18,7.11v7.37h-2.1Z\"/>
                <path class=\"cls-1\" d=\"m384.19,94.22h-6.7v6.22h7.47v1.87h-9.7v-17.25h9.31v1.87h-7.09v5.45h6.7v1.84Z\"/>
            </g>
            <path class=\"cls-2\" d=\"m48.5,7.84c1.35-.99,2.66-1.95,3.95-2.93.11-.09.16-.33.16-.49-.01-.91.5-1.61,1.32-1.76.81-.14,1.53.27,1.8,1.04.29.81-.05,1.54-.85,2.02-.22.13-.41.43-.46.68-.62,2.85-1.22,5.7-1.79,8.56-.09.46-.27.67-.72.76-4.32.87-8.63.87-12.94,0-.41-.08-.61-.27-.7-.71-.57-2.86-1.19-5.71-1.77-8.57-.08-.39-.21-.64-.6-.82-.73-.35-1.01-1.19-.73-1.95.28-.77,1.01-1.17,1.81-1.02.77.14,1.31.83,1.3,1.64,0,.19,0,.48.12.57,1.31.99,2.64,1.96,4,2.96.63-1.44,1.24-2.8,1.82-4.19.07-.16.01-.42-.08-.58-.45-.83-.37-1.64.27-2.19.6-.52,1.48-.51,2.08.01.63.55.71,1.36.25,2.19-.08.15-.15.38-.09.52.59,1.4,1.21,2.78,1.85,4.23Zm4.54-2.43c-1.43,1.06-2.83,2.08-4.22,3.12-.44.33-.7.29-.93-.25-.58-1.38-1.2-2.75-1.79-4.13-.12-.29-.26-.44-.61-.43-.35,0-.54.08-.68.42-.56,1.34-1.16,2.67-1.74,4.01-.32.74-.43.77-1.08.29-1.38-1.01-2.75-2.02-4.13-3.03-.25.16-.47.29-.7.43,0,.12,0,.23.02.33.45,2.15.92,4.3,1.34,6.45.08.42.27.57.65.65,3.71.71,7.43.8,11.16.24.62-.09,1.41-.05,1.79-.42.39-.38.38-1.19.54-1.81,0-.02.01-.04.02-.06.37-1.79.74-3.57,1.11-5.36-.26-.16-.48-.29-.74-.45Zm-.99,8.53c-4.42.91-8.81.9-13.19,0-.06.79.24,1.08.96,1.21,3.77.66,7.52.64,11.29,0,.76-.13.94-.5.94-1.22Zm-5.71-11.84c-.02-.49-.46-.88-.95-.85-.46.03-.84.45-.83.91.02.49.45.88.95.85.47-.03.85-.44.83-.91Zm-9.65,1.26c-.47,0-.86.38-.88.87-.02.51.39.93.89.92.47-.01.85-.42.85-.9,0-.49-.39-.88-.86-.88Zm18.41.91c0-.51-.39-.92-.89-.91-.47.01-.85.4-.85.88,0,.5.4.92.89.91.47,0,.84-.4.85-.88Z\"/>
            <g>
                <path class=\"cls-1\" d=\"m86.19,13.26h-18.36V.61h51.79v12.65h-18.36v54.05h-15.08V13.26Z\"/>
                <path class=\"cls-1\" d=\"m135.15.61h41.79v12.65h-26.71v13.39h22.74v12.65h-22.74v15.37h27.73v12.65h-42.81V.61Z\"/>
                <path class=\"cls-1\" d=\"m192.5.61h24.02c14.15,0,25.74,4.91,25.74,20.77s-11.59,21.92-25.74,21.92h-8.94v24h-15.08V.61Zm22.79,30.72c7.93,0,12.19-3.39,12.19-9.95s-4.26-8.8-12.19-8.8h-7.71v18.75h7.71Zm-1.53,8l10.43-9.65,21.29,37.62h-16.87l-14.84-27.98Z\"/>
                <path class=\"cls-1\" d=\"m261.62.61h15.08v66.7h-15.08V.61Z\"/>
                <path class=\"cls-1\" d=\"m312.51.61h18.04l20.95,66.7h-15.96l-8.73-33.78c-1.85-6.67-3.63-14.55-5.42-21.51h-.41c-1.61,7.04-3.39,14.84-5.25,21.51l-8.76,33.78h-15.42L312.51.61Zm-7.14,39.09h32.09v11.72h-32.09v-11.72Z\"/>
                <path class=\"cls-1\" d=\"m38.35,21.5h15.08v45.74h-15.08V21.5Z\"/>
            </g>
            </g>
        </g>
        </svg>

        </div>
        <div class=\"links mt-5\">
            <ul class=\"navbar-nav me-auto mb-2 mb-lg-0\">

                <li class=\"nav-item my-2\">
                    <a class=\"nav-link active text-white fs-5 text-center\" aria-current=\"page\" href=\"dashboard.ad.php\"> <i class=\"fa-solid fa-1x fa-house-user pe-2\" style=\"color: white;\"></i> <span class=\"side-bar-text d-none d-sm-none d-md-none d-lg-inline\" >DASHBOARD</span></a>
                </li>

                <li class=\"nav-item my-2\">
                    <a class=\"nav-link active text-white fs-5 text-center\" aria-current=\"page\" href=\"orders.ad.php\"> <i class=\"fa-solid fa-1x fa-sheet-plastic pe-2\" style=\"color: white;\"></i><span class=\"side-bar-text d-none d-sm-none d-md-none d-lg-inline\">ORDERS</span></a>
                </li>
                <li class=\"nav-item my-2\">
                    <a class=\"nav-link active text-white fs-5 text-center\" aria-current=\"page\" href=\"update.ad.php\"> <i class=\"fa-solid fa-1x fa-pen-to-square pe-2\" style=\"color: white;\"></i><span class=\"side-bar-text d-none d-sm-none d-md-none d-lg-inline\">UPDATE</span></a>
                </li>
                <li class=\"nav-item my-2\">
                    <a class=\"nav-link active text-white fs-5 text-center\" aria-current=\"page\" href=\"feedback.ad.php\"> <i class=\"fa-solid fa-1x fa-comment pe-2\" style=\"color: white;\"></i> <span class=\"side-bar-text d-none d-sm-none d-md-none d-lg-inline\">FEEDBACK</span></a>
                </li>
                <li class=\"nav-item my-2\">
                    <a class=\"nav-link active text-white fs-5 text-center\" aria-current=\"page\" href=\"login.php?exitsession\"> <i class=\"fa-solid fa-1x fa-right-from-bracket pe-2\" style=\"color: white;\"></i> <span class=\"side-bar-text d-none d-sm-none d-md-none d-lg-inline\">LOG OUT</span></a>
                </li>
            </ul><i class=\"fa-solid \"></i>
        </div>

        </section>";
        echo $side_Bar;
    }
    function draw_orders($orderid, $date, $time, $location, $item, $qty, $itemcount, $contactno){
        if($itemcount ==1){
            $secondt = '';
            $secondq = '';
        }else{
            $secondt = $item[1];
            $secondq = $qty[1];
        }
        $ele = "<tr>
        <th class =\"p-4\" scope=\"row\">$orderid</th>
        <td class =\"p-3\">$date</td>
        <td class =\"p-4\">$time</td>
        <td class =\"p-4\">$location</td>
        <td class =\"p-4\">$contactno</td>
        <td class =\"p-4\">";
        foreach($item as $name){
            $ele .= "$name<br>";
        }
        $ele .= "</td><td class =\"p-4\">";
        foreach($qty as $q){
            $ele .= "$q<br>";
        }
        $ele .= "</td>
        </tr>";
        
        echo $ele;
    
    }
    function draw_feedback($time, $date, $feedback){
        $ele = "
        <div class=\"px-4 m-2 \" style=\"border-radius: 25px; background-color:#F6821F;\">
        <div class=\"position-relative feedback-text text-white p-5 my-4\" >$feedback
        <span class=\"position-absolute feedback-text text-white bottom-0 end-0\" style=\"font-size: .7rem;\">$date <br> $time</span>
        </div>
        </div>";
        echo $ele;
    }



?>