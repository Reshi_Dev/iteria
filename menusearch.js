let userSearch = document.querySelector('#user-query');
let searchBtn = document.querySelector('.search-btn');
let cardDiv = document.querySelector('.card-part');
let cardRowDiv = document.querySelector('.card-row-container');
let nextBtns = document.querySelector('.next-btns');
let nextBtn = document.getElementById('nextt-btn');
let items = [{
        name: "Veg Momo",
        src: "assets/images/momo.png",
        price: 50,
        category: ["ready-to-eat", "veg"]
    },
    {
        name: "Pokora",
        src: "assets/images/pokora.png",
        price: 10,
        category: "ready-to-eat"
    },
    {
        name: "Fried Rice",
        src: "assets/images/friedrice.png",
        price: 35,
        category: "ready-to-eat"
    },
    {
        name: "Milk Tea",
        src: "assets/images/milktea.png",
        price: 15,
        category: "ready-to-eat"
    },
    {
        name: "Aloo Dum",
        src: "assets/images/aloodum.png",
        price: 10,
        category: "ready-to-eat"
    },
    {
        name: "Chana",
        src: "assets/images/chana.png",
        price: 10,
        category: "ready-to-eat"
    },
    {
        name: "Samosa",
        src: "assets/images/samosa.png",
        price: 10,
        category: "ready-to-eat"
    },
    {
        name: "Puri",
        src: "assets/images/puri.png",
        price: 35,
        category: "ready-to-eat"
    },
    {
        name: "Maggie",
        src: "assets/images/maggie.png",
        price: 25,
        category: "ready-to-eat"
    },
    {
        name: "Omlete",
        src: "assets/images/omlete.png",
        price: 25,
        category: "ready-to-eat"
    },
    {
        name: "Water",
        src: "assets/images/water.png",
        price: 10,
        category: "ready-to-eat"
    },
    {
        name: "Ema Datse",
        src: "assets/images/emadatse.png",
        price: 60,
        category: "ready-to-eat"
    },

];

function get(att, arrObj) {
    let arr = [];
    for (let i = 0; i < arrObj.length; i++) {
        arr.push(arrObj[i][att]);
    }
    return arr;
}

userSearch.addEventListener('keypress', function(event) {
        if (event.key === "Enter") {
            event.preventDefault();
            searchBtn.click();
        }
    })
    // SELECT * from items where INSTR(`itemname`, 'oo') > 0;

searchBtn.addEventListener('click', () => {
    cardDiv.innerHTML = '';
    nextBtns.innerHTML = '';
    let count = 0;
    for (let i = 0; i < items.length; i++) {
        if (stringSearch(items[i]['name'].toLowerCase(), userSearch.value)) {
            cardDiv.innerHTML += '<div class="card shadow p-2 m-2 d-inline-block" style="width: 14rem; border-radius: 20px;">' +
                '<div class="d-block" width="222" height="172"></div>' +
                '<img src="' + items[i]['src'] + '" class="card-img-top" width="222" height="142">' +
                '<div class="card-body">' +
                ' <h5 class="card-title">' + items[i]['name'] + '</h5>' +
                '<p class="card-text" style="color: #F6821F;">BTN <span id="item-price">' + items[i]['price'] + '</span></p>' +
                ' <a href="#" class="btn w-100 mt-1 text-white" style="border-radius:15px; background-color: #F6821F;"> <i class="fas fa-shopping-cart me-2"></i> ADD TO CART</a>' +
                ' </div>' +
                '</div>';
            count += 1;
        }
    }
    if (count === 0) {
        cardDiv.innerHTML += '<div class = "text-dark fs-3 text-normal" > <br> No results containing all your search terms were found. <br> Your search - <span class = "fs-1">' + userSearch.value + '</span> - did not match any documents.<div/>';
    }
})

function stringSearch(string, search) {
    let count = 0;
    for (let i = 0; i < string.length; i++) {
        for (let j = 0; j < search.length; j++) {
            if (search[j] !== string[i + j]) break;
            if (j === search.length - 1) count++;
        }
    }
    if (count >= 1) {
        return true;
    } else {
        return false;
    }
}

function displayPrev() {
    location.reload()
}

function displayNext() {
    cardDiv.innerHTML = '';
    nextBtns.innerHTML = '';
    for (let i = 9; i < items.length; i++) {
        cardDiv.innerHTML += '<div class="card shadow p-2 m-2 d-inline-block" style="width: 14rem; border-radius: 20px;">' +
            '<div class="d-block" width="222" height="172"></div>' +
            '<img src="' + items[i]['src'] + '" class="card-img-top" width="222" height="142">' +
            '<div class="card-body">' +
            ' <h5 class="card-title">' + items[i]['name'] + '</h5>' +
            '<p class="card-text" style="color: #F6821F;">BTN <span id="item-price">' + items[i]['price'] + '</span></p>' +
            ' <a href="#" class="btn w-100 mt-1 text-white" style="border-radius:15px; background-color: #F6821F;"> <i class="fas fa-shopping-cart me-2"></i> ADD TO CART</a>' +
            ' </div>' +
            '</div>';
    }
    nextBtns.innerHTML += '<div class="navigate-btn btn"> <button id="#nextt-btn" class="btn rounded-pill px-3 ms-3 text-white" style="background-color:#F6821F; " onclick="displayPrev();">PREV</button></div> '

}