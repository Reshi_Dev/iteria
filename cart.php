<?php
include_once 'component.php';
include_once 'connection.php';

session_start();

if(!isset($_SESSION['currentuser'])){
    header("Location: ../ITERIA/login.php?loginagain");
    exit();
}





?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CART</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Sarala&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="index.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer"
    />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer"
    />
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@700&family=Sarala&display=swap');
        * {
            font-family: 'Poppins', sans-serif;
            box-sizing: border-box;
        }
        
        .nav-link {
            font-weight: 700;
        }
        
        body {
            height: 100vh;
        }
        
        footer {
            font-weight: 100;
            font-size: 15px;
            color: white;
            background-color: #F6821F;
        }
        
        .banner {
            background-image: url('assets/images/banner.png');
            background-size: cover;
            background-repeat: no-repeat;
        }
        /* for page css */
    </style>
</head>

<body class="mt-5 d-flex flex-column min-vh-100">
    <?php
        include_once 'clientparts.php';
        $imgpath = $_SESSION['currentuserimgpath'];
        draw_nav_bar($imgpath);
    ?>

    <main style="background-color: #ededed;">



        <div class="banner mt-4 p-5 w-100">
            
            <div class="position-relative fs-4 w-100 text-center"></div>
        </div>
        
        <div class="container my-5">
        <div class="ps-4 my-4">
                    <?php 
                        if(isset($_GET['emptyinput'])){
                            echo "<span class=\"text-danger fs-5\">All fields are requried to place order</span>";
                        }if(isset($_GET['closed'])){
                            echo "<span class=\"text-danger fs-5\">Sorry we could not process your order</span>";
                        }if(isset($_GET['emptycart'])){
                            echo "<span class=\"text-danger fs-5\">Your cart is empty, please add items in your cart</span>";
                        }if(isset($_GET['incorrectdate'])){
                            echo "<span class=\"text-danger fs-5\">Invalid date</span>";
                        }if(isset($_GET['incorrecttime'])){
                            echo "<span class=\"text-danger fs-5\">Time should be between 9am - 5pm</span>";
                        }
                    ?>
                    </div>
            <div class="row d-flex justify-content-center align-items-center h-100">
                <div class="col-12">
                    
                    <div class="card card-registration card-registration-2" style="border-radius: 15px;">
                        <div class="card-body p-0">
                            <div class="row g-0">
                                <div class="col-lg-8">
                                    <div class="p-5">
                                        <div class="d-flex justify-content-between align-items-center mb-5">
                                            <h1 class="fw-bold mb-0 text-black text-center" style="color:#F6821F;">Your Cart</h1>
                                            <h6 class="mb-0 text-muted"><span id="items-count"> <?php
                                            
                                            if(isset($_POST['remove'])){
                                                // echo $_GET['id'];
                                                if($_GET['action']=='remove'){
                                                    foreach($_SESSION['cart'] as $key => $value){
                                                        if($value['itemID'] == $_GET['id']){
                                                            // echo $key;
                                                            unset($_SESSION['cart'][$key]);
                                                            // header("Location: ../ITERIA/cart.php?succesfullyremoved");
                                                            // exit();
                                                        }
                                                    }
                                                }
                                            }
                                            
                                                 
                                            if (isset($_SESSION['cart'])){
                                                $count = count($_SESSION['cart']);
                                                
                                            }else{
                                                $count = 0;
                                            }
                                            echo $count;
                                        ?>
                                        </span> items</h6>
                                        </div>

                                        <hr class="my-4">
                                            <!-- YOUR CART -->
                                            <?php 
                                            include_once 'connection.php';
                                            if(isset($_GET['orderplaced'])){
                                                echo "<span class=\"fs-5 text-success\">YOUR ORDER HAS BEEN PLACED</span><br>";
                                                $sql = "SELECT * from orders order by orderid DESC LIMIT 1;";
                                                $result = $conn->query($sql);
                                                
                                                while ($row = mysqli_fetch_assoc($result)){
                                                    $order_id = $row['orderid'];
                                                    echo "<span class=\"fs-5 text-dark\">ORDER ID : $order_id</span>";
                                                }
                                            }
                                            $grand_total = 0;
                                            $total = 0;
                                            
                                            if(isset($_SESSION['cart'])){

                                                if((int)$_SESSION['cart'] == 0){
                                                    echo "<h5 >Your ITERIA basket is empty </h5>";
                                                }
                                                else{
                                                        $product_id = array_column($_SESSION['cart'], 'itemID');
                                                        $sql = "select * from items";
                                                        $result = $conn->query($sql);
                                                        $i = 0;
                                                    while ($row = mysqli_fetch_assoc($result)){
                                                        foreach ($product_id as $id){
                                                            if ($row['itemid'] == $id){
                                                                if(isset($_POST['qty'])){
                                                                    if($_GET['qtyid'] == $row['itemid']){
                                                                        $total = $total + ((int)$row['itemprice']*$_POST['qty']); 
                                                                    }else{
                                                                        if(isset($_SESSION['cart'][$i]['qty'])){
                                                                            $total = $_SESSION['cart'][$i]['qty'];
                                                                        }else{
                                                                            $total = $total + (int)$row['itemprice'];
                                                                        }
                                                                    }
                                                                
                                                                    $_SESSION['cart'][$i]['qty'] = $total;
                                                                }                       
                                                                
                                                                if(isset($_SESSION['cart'][$i]['qty'])){
                                                                    draw_cart_components($row['itemname'], $row['itemprice'], $row['itemimg'], $row['itemid'], $_SESSION['cart'][$i]['qty']);
                                                                }else{
                                                                    $_SESSION['cart'][$i]['qty'] = $row['itemprice'];
                                                                    draw_cart_components($row['itemname'], $row['itemprice'], $row['itemimg'], $row['itemid'], $_SESSION['cart'][$i]['qty']);
                                                                }
                                                                // print_r($_SESSION['cart'][$i]['qty']);
                                                                $i = $i + 1;
                                                                $total = 0;
                                                            }
                                                        }
                                                    }
                                                    if(isset($_SESSION['cart'])){
                                                        for($i = 0; $i < count($_SESSION['cart']); $i++){
                                                            if(isset($_SESSION['cart'][$i]['qty']) && !isset($_SESSION['cart'][$i]['itemID'])){
                                                                // echo "ok";
                                                                unset($_SESSION['cart'][$i]);
                                                    
                                                            }
                                                        }
                                                    }
                                                   
                                                    
                                                    if(isset($_SESSION['cart'][0]['qty'])){
                                                        foreach($_SESSION['cart'] as $item){
                                                            // print_r($item);
                                                            $grand_total += $item['qty'];
                                                        }
                                                    }
                                                    // print_r($_SESSION['cart']);
                                                    if($grand_total == 0 && isset($_SESSION['cart'])){
                                                    //    echo $grand_total;
                                                        foreach ($_SESSION['cart'] as $z){
                                                            $cart_item_id = $z['itemID'];
                                                            $sql = "select * from items where itemid='$cart_item_id'";
                                                            $result = $conn->query($sql);
                                                            $userdata = $result->fetch_assoc();
                                                            $grand_total += $userdata['itemprice'];
                                                        }
                                                    }
                                                }
                                                // print_r($_SESSION['cart']);  
                                            }
                                            
                                            // print_r($_SESSION['user']);
                                            ?>  
                                        <div class="pt-5">
                                            <h6 class="mb-0"><a href="menu.php" class="btn text-body text-decoration-none" style="background:#F6821F;">
                                                <i class="fas fa-long-arrow-alt-left me-2" style="color:white;"></i>
                                                <span class="text-white">Back to Menu</span>  
                                            </a></h6>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 text-white" style="background-color:#F6821F;">
                                    <div class="p-5">
                                        <h3 class="fw-bold mb-5 mt-2 pt-1">Summary</h3>
                                        <hr class="my-4">

                                        <div class="d-flex justify-content-between mb-4">
                                            <h5 class="text-uppercase">you have <span id="items-count"><?php
                                                    
                                                    if (isset($_SESSION['cart'])){
                                                        $count = count($_SESSION['cart']);  
                                                    }else{
                                                        $count = 0;
                                                    }
                                                    
                                                    echo $count;

                                                    

                                                ?> Items</span></h5>
 
                                        </div>
                                        <form action="placeorder.php" method="post">
                                        <h5 class="text-uppercase mb-3">location</h5>

                                        <div class="mb-4 pb-2">
                                            <select class="select" name="location">
                                            <option value="Canteen">Canteen</option>
                                            <option value="Classroom 1">Classroom 1</option>
                                            <option value="MPH">MPH</option>
                                            <option value="Lab 3">Lab3</option>
                                            <option value="Finance Section">Finance Section</option>

                                            </select>
                                        </div>

                                        <h5 class="text-uppercase mb-3">Date</h5>
                                        <div class="mb-5">
                                            <div class="form-outline">
                                                <input type="date" name="date" id="form3Examplea2" class="form-control" />

                                            </div>
                                        </div>
                                        <h5 class="text-uppercase mb-3">Time</h5>
                                        <div class="mb-5">
                                            <div class="form-outline">
                                                <input type="time" name="time" id="form3Examplea2" class="form-control" />

                                            </div>
                                        </div>

                                        <hr class="my-4">

                                        <div class="d-flex justify-content-between mb-5">
                                            <h5 class="text-uppercase ">Total price</h5>
                                            <h5>BTN <span class= "itemprice "><?php
                                                echo $grand_total;
                                            ?></span></h5>
                                        </div>
                                        <input type="hidden" value="<?php echo $grand_total?>" name ="total">
                                        <button type="submit" class="btn btn-block w-100 " name="submitorder" data-mdb-ripple-color="dark" style="height:45px; border-radius: 20px; background-color: white; color:#F6821F;">Place Order</button>
                                        </form>
                                       

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <?php
        draw_footer();
        
    ?>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    <script src="cart.js"></script>

</body>

</html>