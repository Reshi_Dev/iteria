<?php
    include_once 'connection.php';
    session_start();
    $current_user_id = $_SESSION['currentuser'];
    $sql = "select * from users where uid = $current_user_id;";
    $result = $conn->query($sql);
    $userdata = $result->fetch_assoc();  
    $uid = $userdata['uid'];
    $name = $userdata['name'];
    $email = $userdata['email'];
    $contactno = $userdata['contactno'];
    $imgpath = $userdata['imgpath'];
    
    
?>


<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Account Setting</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Sarala&display=swap" rel="stylesheet">
    <link
      class="jsbin"
      href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/base/jquery-ui.css"
      rel="stylesheet"
      type="text/css"
    />
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

    <style>
       @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@700&family=Sarala&display=swap');
        body {
            background: #f9f9f9;
            font-family: 'Poppins', sans-serif;
        }

        .shadow {
            box-shadow: 0 2px 10px rgba(0, 0, 0, 0.1) !important;
        }

        .profile-tab-nav {
            min-width: 250px;
        }

        .tab-content {
            flex: 1;
        }

        .form-group {
            margin-bottom: 1.5rem;
        }

        .nav-pills a.nav-link {
            padding: 15px 20px;
            border-bottom: 1px solid #ddd;
            border-radius: 0;
            color: black;
        }
        .nav-pills a.nav-link i {
            width: 20px;
        }

        .img-circle img {
            height: 100px;
            width: 100px;
            border-radius: 100%;
            border: 5px solid #fff;
        }
        #v-pills-tab > .active{
            background-color: #F6821F !important;
        }
        /* #blah{
            border-radius:500px;
        } */
		.main-page-btn:hover{
			background-color: #F6821F;
			color: white !important;
		}

    </style>
        <script
      class="jsbin"
      src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"
    ></script>
    <script
      class="jsbin"
      src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.0/jquery-ui.min.js"
    ></script>
</head>
<body>
	<section class="py-5 my-5">

		<div class="container">
			<h2 class="mb-5 text-center">USER SETTING</h2>
			<?php 
				if(isset($_GET['updateprofile'])){
					if($_GET['updateprofile'] == 'failed'){
						echo "<div class=\"w-100 py-2 text-center shadow border\">
						<span class=\"text-danger fs-4 w-100 text-center mx-5 px-2\">Please select a image</span>
					</div>";
					}else{
						echo "<div class=\"w-100 py-2 text-center shadow border\">
						<span class=\"text-success fs-4 w-100 text-center mx-5 px-2\">Profile updated!</span>
					</div>";
					}
				}else if(isset($_GET['error'])){
					if($_GET['error'] == 'wrongpassword'){
						echo "<div class=\"w-100 py-2 text-center shadow border\">
						<span class=\"text-danger fs-4 w-100 text-center mx-5 px-2\">Your password is incorrect</span>
					</div>";
					}else if($_GET['error'] == 'invalidname'){
						echo "<div class=\"w-100 py-2 text-center shadow border\">
						<span class=\"text-danger fs-4 w-100 text-center mx-5 px-2\">Invalid name!</span>
					</div>";}
					else if($_GET['error'] == 'invalidemail'){
						echo "<div class=\"w-100 py-2 text-center shadow border\">
						<span class=\"text-danger fs-4 w-100 text-center mx-5 px-2\">Invalid email!</span>
					</div>";}
					else if($_GET['error'] == 'emailinuse'){
						echo "<div class=\"w-100 py-2 text-center shadow border\">
						<span class=\"text-danger fs-4 w-100 text-center mx-5 px-2\">Email already registered</span>
					</div>";}
					else{
						echo "<div class=\"w-100 py-2 text-center shadow border\">
						<span class=\"text-danger fs-4 w-100 text-center mx-5 px-2\">Passwords do not match</span>
						</div>";
					}
				}else if(isset($_GET['useraccountupdate'])){
					echo "<div class=\"w-100 py-2 text-center shadow border\">
						<span class=\"text-success fs-4 w-100 text-center mx-5 px-2\">Updated</span>
						</div>";
				}
			?>

			
			<div class="bg-white shadow-lg rounded-lg d-block d-sm-flex ">
				
				<div class="profile-tab-nav border-right">
					<div class="p-4 ">
						<div class="img-circle text-center mb-3">
							<img src="<?php echo $imgpath;?>" alt="user image" class="shadow">
						</div>
						<h4 class="text-center"><?php echo $name;?></h4>
                        <h5 class="text-muted text-center " style=" width:100%;font-size:11px;"><?php echo $email;?></h5>
					</div>
					<div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
						<a class="nav-link active" id="account-tab" data-toggle="pill" href="#account" role="tab" aria-controls="account" aria-selected="true">
							<i class="fa fa-home text-center mr-1"></i> 
							Account
						</a>
						<a class="nav-link" id="password-tab" data-toggle="pill" href="#password" role="tab" aria-controls="password" aria-selected="false">
							<i class="fa fa-key text-center mr-1"></i> 
							Password
						</a>
						<a class="nav-link " id="profile-tab" data-toggle="pill" href="#profile" role="tab" aria-controls="profile" aria-selected="false">
							<i class="fa fa-user text-center mr-1"></i> 
							Profile
						</a>
					</div>
				</div>
				<div class="tab-content p-4 p-md-5" id="v-pills-tabContent">
    

					<div class="tab-pane fade show active" id="account" role="tabpanel" aria-labelledby="account-tab">
						<h3 class="mb-4">Account Settings</h3>
                        <form action="updateaccount.php" method="post">
                        <div class="row">
							<div class="col-md-6">
								<div class="form-group">
								  	<label>Name</label>
								  	<input type="text" name="newname" class="form-control" value="<?php echo $name;?>">
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
								  	<label>Phone number</label>
								  	<input type="text" name="newcontactno" class="form-control" value="<?php echo $contactno;?>">
								</div>
							</div>
						</div>
						<div>
							<button class="btn text-white" type="submit" name="submit" style="background-color: #F6821F;">Update</button>
						</div>
                        </form>
						
					</div>

                    <!-- newtab -->

					<div class="tab-pane fade" id="password" role="tabpanel" aria-labelledby="password-tab">
						<h3 class="mb-4">Password Settings</h3>
                        <form action="updatepassword.php" method="post">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
								  	<label>Old password</label>
								  	<input type="password" name="opwd" class="form-control">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
								  	<label>New password</label>
								  	<input type="password" name="npwd" class="form-control">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
								  	<label>Confirm new password</label>
								  	<input type="password" name="cpwd" class="form-control">
								</div>
							</div>
						</div>
						<div>
							<button class="btn text-white" type="submit" name="submit" style="background-color: #F6821F;">Update</button>
						</div>
                        </form>
					</div>

                    <!-- newtab -->

					<div class="tab-pane fade " id="profile" role="tabpanel" aria-labelledby="profile-tab">
                    <h3 class="mb-4">Profile</h3>
						<span class="text-muted">Select a photo</span>
                        <form action="updateprofile.php" method="post" enctype="multipart/form-data">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group img-upload">
                                
                                    <label for="file-input">
                                        <img src="assets/images/imginput.png" width="100" height="100"/>
										<!-- <i class="fas fa-image"></i> -->
                                    </label>
                                    <input id="file-input" class="d-none" name="userimage" type="file" onchange="readURL(this);"/>
                                
								</div>
                                
							</div>
                            <div class="col-md-6 ">
                                <img id="blah" src=""/>
                            </div>
						
						</div>
						<div class="">
							<button class="btn text-white" type="submit" name="submit" style="background-color: #F6821F;">Update</button>
						</div>
                        </form>
					</div>
				</div>
				
			</div>
			<div class="w-100 text-center shadow border">
				<a href="index.php" class="btn main-page-btn shadow btn-light  w-100 p-3">Back to main page</a>
			</div>
			
		</div>
	</section>


	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

    <script>
        function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
            $('#blah').attr('src', e.target.result).width(150).height(190);
            };

            reader.readAsDataURL(input.files[0]);
        }
        }
    </script>
</body>
</html>