<?php
    session_start();
    if(!isset($_SESSION['currentuser'])){
        header("Location: ../ITERIA/login.php?loginagain");
        exit();
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MENU</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Sarala&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer"
    />


    <style>
        @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@700&family=Sarala&display=swap');
        * {
            font-family: 'Poppins', sans-serif;
            box-sizing: border-box;
        }
        
        .nav-link {
            font-weight: 700;
        }
        
        body {
            height: 100vh;
        }
        
        footer {
            font-weight: 100;
            font-size: 15px;
            color: white;
            background-color: #F6821F
        }
        
        .banner {
            background-image: url('assets/images/banner1.png');
            background-size: cover;
            background-repeat: no-repeat;
        }
        /* for page css */
        
        .option {
            background-color: #EEEEEE;
        }
        
        .option a span {
            text-align: center;
        }
        
        .free-coke {
            background-color: #F6821F;
            border-radius: 40px;
        }
        
        .btn-order-now {
            background-color: white;
            color: #F6821F;
        }
        
        .search-icon {
            color: #F6821F;
            background-color: #EEEEEE;
        }
        
        #search-addon {
            position: absolute;
            top: 4px;
            left: 5px;
            z-index: 100;
        }

        
        @media (min-width: 850px) and (max-width: 1100px) {
            h4{
                font-size: 20px !important;
            }
            p{
                font-size: 14px !important;  
            }
            .card{
                width: 11rem !important;
            }
            .card-btn{
                font-size: 11px !important;
            }
            .item-price, .card-text{
                font-size: 13px !important;
            }
            .card-title{
                font-size: 17px !important;
            }
            .card-img-top{
                width: 150px;
                height: 120px;
            }
            footer {
                font-size: 15px !important;
            }
        }
        
        @media (max-width: 850px) {
            h4{
                font-size: 18px !important;
            }
            p{
                font-size: 12px !important;  
            }
            .card{
                width: 9rem !important;

            }
            .card-btn{
                font-size: 9px !important;
            }
            .item-price, .card-text{
                font-size: 9px !important;
            }
            .card-title{
                font-size: 11px !important;
            }
            .card-img-top{
                width: 120px;
                height: 90px;
            }

            footer {
                font-size: 13px !important;
            }
        }
        .box {
            animation: rotateY-anim 5s linear infinite;
            
        }

        @keyframes rotateY-anim {
            0% {
                transform: rotateY(0deg);
            }

            100% {
                transform: rotateY(360deg);
            }
        }
    </style>
</head>

<body class="mt-5 d-flex flex-column min-vh-100">
    <?php
        include_once 'clientparts.php';
        $imgpath = $_SESSION['currentuserimgpath'];
        draw_nav_bar($imgpath);     
        
    ?>

    <main>
        <!-- image of banner -->
        <div class="banner mt-4 p-5 w-100">
            <div class="position-relative fs-4 w-100 text-center text-white">MENU</div>
        </div>
        <!-- The main content -->
        <div class="container main-content-dd w-100 h-100 p-4">
            <div class="row d-felx justify-content-around flex-row ">
                <div class="left-part col p-2 d-flex flex-column justify-content-start mt-4">
                    <div class="option-bar d-felx flex-row justify-content-around">
            
                        <form action="menu.php" method="get">
                        <div class="option p-3 m-1 d-flex flex-column border rounded-pill my-3">
                            <button type="submit" id="rte" name="category" value ="readytoeat" class="text-decoration-none d-flex justify-content-start " <?php echo ((isset($_GET['category']) && $_GET['category'] == 'readytoeat')) ? 'style="border:none; color:#F6821F;font-size:17px;"' : 'style="border:none;"'; ?>> <img class="img-fluid px-2" src="assets/images/readytoeat.png" alt="" width="45"> READY-TO-EAT</button>
                        </div>
                        </form>
                        <form action="menu.php" method="get">
                        <div class="option p-3 m-1 d-flex flex-column border rounded-pill my-3">
                            <button type="submit" id="ld" name="category" value ="lunchdinner" class="text-decoration-none  d-flex justify-content-start" <?php echo ((isset($_GET['category']) && $_GET['category'] == 'lunchdinner')) ? 'style="border:none; color:#F6821F; font-size:17px;"' : 'style="border:none;"'; ?>><img class="img-fluid px-2" src="assets/images/lunchdinner.png" alt="" width="45"> LUNCH/DINNER</button>
                        </div>
                        </form>
                        <form action="menu.php" method="get">
                            
                        <div class="option p-3 m-1 d-flex flex-column border rounded-pill my-3">
                            <button type="submit" id="d" name="category" value ="drinks" class="text-decoration-none  d-flex justify-content-start" <?php echo ((isset($_GET['category']) && $_GET['category'] == 'drinks')) ? 'style="border:none; color:#F6821F;font-size:17px;"' : 'style="border:none;"'; ?>><img class="img-fluid px-2" src="assets/images/drinks.png" alt="" width="45"> DRINKS</button>
                        </div>
                        </form>
                        <form action="menu.php" method="get">
                        <div class="option p-3 m-1 d-flex flex-column border rounded-pill my-3">
                            <button type="submit" id="g"  name="category" value ="groceries" class="text-decoration-none  d-flex justify-content-start" <?php echo ((isset($_GET['category']) && $_GET['category'] == 'groceries')) ? 'style="border:none; color:#F6821F;font-size:17px;"' : 'style="border:none;"'; ?>><img class="img-fluid px-2" src="assets/images/groceries.png" alt="" width="45"> GROCERIES</button>
                        </div>
                        </form>    
                        <form action="menu.php" method="get">
                        <div class="option p-3 m-1 d-flex flex-column border rounded-pill my-3">
                            <button type="submit" id="v" name="category" value ="veg" class="text-decoration-none  d-flex justify-content-start" <?php echo ((isset($_GET['category']) && $_GET['category'] == 'veg')) ? 'style="border:none; color:#F6821F;font-size:17px;"' : 'style="border:none;"'; ?>><img class="img-fluid px-2" src="assets/images/veg.png" alt="" width="45"> VEG</button>
                        </div>
                        </form> 
                        <form action="menu.php" method="get">
                        <div class="option p-3 m-1 d-flex flex-column border rounded-pill my-3">
                            <button type="submit" id="n" name="category" value ="nonveg" class="text-decoration-none  text d-flex justify-content-start" <?php echo ((isset($_GET['category']) && $_GET['category'] == 'nonveg')) ? 'style="border:none; color:#F6821F;font-size:17px;"' : 'style="border:none;"'; ?>><img class="img-fluid px-2" src="assets/images/nonveg.png" alt="" width="45">NON-VEG </button>
                        </div>
                        </form> 
                    </div>
                    <div class="free-coke d-felx d-md-none d-none d-sm-none d-lg-none d-xl-block justify-content-around align-items-center p-3 " >
                        <div><img class="img-fluid mt-3" src="assets/images/freecoketea.png" alt="" width="200"></div>
                        <div>
                            <p class="text-white text-center mt-4 mb-5 fs-3">Get a free <br> drink from <br> your first <br> online order</p>
                            
                        </div>
                        <div class="box"><img class="img-fluid mb-4" src="assets/images/freecoke.png" alt="" width="250"></div>
                    </div>

                </div>

                <div class="col-9 p-5 ">
                    <div class="search-bar mb-4">
                        <form action="menu.php" method="get">
                        <div id="search-box" class="input-group rounded srcbr position-relative" style="border-radius: 50px;">
                            <input type="search" id="live_search" name ="search" class="form-control text-black search-b ps-5" style="background-color:#EEEEEE; border-radius: 20px; height: 45px;" placeholder="Search food item" aria-label="Search" aria-describedby="search-addon" />
                            <span class="input-group-text search-icon btn search-btn" style="border-radius: 20px;" id="search-addon">
                                    <i class="fas fa-search" ></i>
                            </span>
                        </div>
                        </form>
                    </div>
                    <div class="text-danger my-3 w-100 d-flex justify-content-center">
                        <?php
                        include_once 'component.php';
                        // include_once 'connection.php';

                        if(isset($_GET['error'])){
                            if($_GET['error'] == 'alreadyincart'){
                                echo "<span class =\"text-danger\">Item has been already added</span>";
                                
                            }
                        }
                        else if(isset($_GET['succesfullyadded'])){
                            echo "<span class =\"text-success\">Added in your cart</span>";
                        }
                        ?>
                    </div>
                    <section class="card-part w-100" id="card-main-div">
                        
                        <?php
                        // print_r($_SESSION['query']);
                            if(isset($_POST['next'])){
                                $navigate['start'] = 10;
                                $count_query = $conn->query("select * from items");
                                $n = (int) mysqli_num_rows($count_query) + 1;
                                $navigate['end'] = $n;
                            }else{
                                $navigate = array('start' => 1, 'end' => 10);
                            }
                            if(isset($_GET['search'])){
                                $user_query = $_GET['search'];
                                $sql = "SELECT * from items where INSTR(`itemname`, '$user_query') > 0;";
                                $result = $conn->query($sql);
                                if(mysqli_num_rows($result) > 0){    
                                    while ($row = mysqli_fetch_assoc($result)){
                                        draw_components($row['itemname'], $row['itemprice'], $row['itemimg'], $row['itemid']);
                                    }
                                }else{
                                    echo  '<div class = "text-dark fs-5 ps-4 text-normal" > <br> No results containing all your search terms were found. <br> Your search - <span class = "fs-2">'.$user_query.'</span> - did not match any documents.<div/>';
                                }
                                
                            }else if(isset($_GET['category'])){
                                if($_GET['category'] == "readytoeat"){

                                    $sql = "SELECT * FROM items where itemstatus = 1;";
                                    $result = $conn->query($sql);
                                    if(mysqli_num_rows($result) > 0){    
                                        while ($row = mysqli_fetch_assoc($result)){
                                            draw_components($row['itemname'], $row['itemprice'], $row['itemimg'], $row['itemid']);
                                        }
                                    }else{
                                        echo  '<div class = "text-dark fs-5 ps-4 text-normal" > <br> Sorry there is nothing to eat!!! :((( <div/>';
                                    }

                                }
                                else if($_GET['category'] == "lunchdinner"){
                                    $sql = "SELECT * FROM items;";
                                    $result = $conn->query($sql);
                                    while ($row = mysqli_fetch_assoc($result)){
                                        $arr = json_decode($row['itemcategory'], true);
                                        if (in_array("lunchdinner", $arr)) {
                                            draw_components($row['itemname'], $row['itemprice'], $row['itemimg'], $row['itemid']);
                                        }
                                    }
                                }else if($_GET['category'] == "drinks"){
                                    $sql = "SELECT * FROM items;";
                                    $result = $conn->query($sql);
                                    while ($row = mysqli_fetch_assoc($result)){
                                        $arr = json_decode($row['itemcategory'], true);
                                        if (in_array("drinks", $arr)) {
                                            draw_components($row['itemname'], $row['itemprice'], $row['itemimg'], $row['itemid']);
                                        }
                                    }
                                }
                                else if($_GET['category'] == "groceries"){
                                    $sql = "SELECT * FROM items;";
                                    $result = $conn->query($sql);
                                    while ($row = mysqli_fetch_assoc($result)){
                                        $arr = json_decode($row['itemcategory'], true);
                                        if (in_array("groceries", $arr)) {
                                            draw_components($row['itemname'], $row['itemprice'], $row['itemimg'], $row['itemid']);
                                        }
                                    }
                                }
                                else if($_GET['category'] == "veg"){
                                    $sql = "SELECT * FROM items;";
                                    $result = $conn->query($sql);
                                    while ($row = mysqli_fetch_assoc($result)){
                                        $arr = json_decode($row['itemcategory'], true);
                                        if (in_array("veg", $arr)) {
                                            draw_components($row['itemname'], $row['itemprice'], $row['itemimg'], $row['itemid']);
                                        }
                                    }
                                }
                                else if($_GET['category'] == "nonveg"){
                                    $sql = "SELECT * FROM items;";
                                    $result = $conn->query($sql);
                                    while ($row = mysqli_fetch_assoc($result)){
                                        $arr = json_decode($row['itemcategory'], true);
                                        if (in_array("nonveg", $arr)) {
                                            draw_components($row['itemname'], $row['itemprice'], $row['itemimg'], $row['itemid']);
                                        }
                                    }
                                }
                            }
                            
                            else{
                                
                                for ($i = $navigate['start']; $i < $navigate['end']; $i++){
                                    $query = "select * from items where itemid = '$i'";
                                    $result = $conn->query($query);
                                    $itemdata = $result->fetch_assoc();
                                    $id = $i;
                                    $img = $itemdata['itemimg'];
                                    $name = $itemdata['itemname'];
                                    $price = $itemdata['itemprice'];
                                    draw_components($name, $price, $img, $id);
                                }
                            }

                        ?>
                    </section>
                    <div class="next-btns d-flex flex-row my-4">
                        <?php
                        include_once 'component.php';

                        if(isset($_POST['next'])){
                            draw_prev_btn();
                            // draw_next_btn();
                        }else if(!isset($_POST['next']) && !isset($_GET['category']) && !isset($_GET['search'])){
                            draw_next_btn();
                        }
                        
                        ?>
                       
                    </div>

                </div>
            </div>
        </div>

    </main>

    <?php
       draw_footer();
    ?>



    
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    <!-- <script src="menusearch.js"></script> -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script>
        // $(document).ready(function(){
        //     $("#live_search").keyup(function(){
        //         var input = $(this).val();
        //         // alert(input);
        //         if(input != ''){
        //             $.ajax({
        //                 url:"menusearch.php",
        //                 method:"POST", 
        //                 data:{"query":input}

        //             })
        //         }else{
        //             $("#card-main-div").css("display", "none");
        //         }
        //     });
        // });
    //     $(document).ready(function(){
    //         $('#rte').click(function() {
    //         $.ajax({
    //             type: "GET",
    //             url: 'menu.php',
    //             data: {name: 'Wayne'},
    //             success: function(data){
    //                 // alert(data);
                    

    // }
    //         })

    //         });
    //     });
       
    </script>
</body>

</html>