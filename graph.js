const labels = ['week 1', 'week 2', 'week 3', 'week 4', 'week 5', 'week 6', 'week 7', 'week 8', 'week 9', 'week 10', 'week 11', 'week 12', 'week 13', 'week 14', 'week15'];
const data = {
    labels: labels,
    datasets: [{
        label: 'Sales',
        backgroundColor: '#F6821F',
        borderColor: '#F6821F',
        data: [0, 1, 5, 2, 20, 30, 42],
    }]
};
const config = {
    type: 'line',
    data: data
};

var myChart = new Chart(document.getElementById('myChart').getContext('2d'), config);