<?php
    session_start();
    include_once 'connection.php';
    include_once 'functions.inc.php';
    if(isset($_POST['submit'])){
        $oldpassword = $_POST['opwd'];
        $newpassword = $_POST['npwd'];
        $confirmpassword = $_POST['cpwd'];
        $current_user_id = $_SESSION['currentuser'];
        $sql = "select * from users where uid = $current_user_id;";
        $result = $conn->query($sql);
        $userdata = $result->fetch_assoc();  
        $database_password = $userdata['password'];


        if(!password_verify($oldpassword, $database_password)){
            header("Location: ../ITERIA/usersetting.php?error=wrongpassword");
            exit();
        }else if(!match_password($newpassword, $confirmpassword)){
            header("Location: ../ITERIA/usersetting.php?error=passwordmismatch");
            exit();
        }else{
            $hashed_pwd = password_hash($newpassword, PASSWORD_DEFAULT);
            echo $hashed_pwd;
            echo "<br>";
            echo $database_password;
            $query = "UPDATE users SET password = '$hashed_pwd' WHERE uid = $current_user_id";
            mysqli_query($conn, $query);
            header("Location: ../ITERIA/login.php?passwordupdate=success");
            exit();
        }
        
    }
?>