<?php
//start session
include_once 'component.php';
include_once 'connection.php';
session_start();

if (isset($_POST['add'])){

    if(isset($_SESSION['cart'])){

        $item_array_id = array_column($_SESSION['cart'], "itemID");

        if(in_array($_POST['itemID'], $item_array_id)){
            header("Location: ../ITERIA/menu.php?error=alreadyincart");
            exit();
        }

        else{

            $count = count($_SESSION['cart']);
            $item_array = array(
                'itemID' => $_POST['itemID']
            );
            $_SESSION['cart'][$count] = $item_array;
            header("Location: ../ITERIA/menu.php?succesfullyadded");
            exit();
        }

    }else{

        $item_array = array(
                'itemID' => $_POST['itemID']
        );
        // Create new session variable
        $_SESSION['cart'][0] = $item_array;
        print_r($_SESSION['cart']);
        header("Location: ../ITERIA/menu.php?succesfullyadded");
        exit();

    }

}
?>