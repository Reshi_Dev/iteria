<?php
    // session_start();        
        

    function draw_nav_bar($imgpath){
        
        if(isset($_SESSION['cart'])){
            $items_count = 0;
            foreach ($_SESSION['cart'] as $Item){
                $items_count += 1;
            }
        }

        ?>
        <style>
            .dark-mode {
            background-color: black;
            color: white;
            
        }

        </style>
        <?php
        $nav = "<nav class=\"navbar navbar-expand-lg navbar-light fixed-top bg-light shadow p-3\">
        <div class=\"container-fluid\">
            <div class=\"container-fluid d-none d-xl-block d-lg-none\">
                <a class=\"navbar-brand\" href=\"index.php\">
                    <svg id=\"Layer_2\" data-name=\"Layer 2\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 384.04 105.2\" alt=\"\" width=\"120\" height=\"85\" class=\"d-inline-block h-100\">
                        <defs>
                        <style>
                            .cls-1, .cls-2 {
                            fill: #f93;
                            }
                    
                            .cls-2 {
                            stroke: #f93;
                            stroke-miterlimit: 10;
                            }
                        </style>
                        </defs>
                        <g id=\"Layer_1-2\" data-name=\"Layer 1\">
                        <g>
                            <g>
                            <path class=\"cls-1\" d=\"m.56,99.35c1,.61,2.45,1.12,3.98,1.12,2.27,0,3.6-1.2,3.6-2.94,0-1.61-.92-2.53-3.24-3.42-2.81-1-4.54-2.45-4.54-4.88,0-2.68,2.22-4.67,5.57-4.67,1.76,0,3.04.41,3.8.84l-.61,1.81c-.56-.31-1.71-.82-3.27-.82-2.35,0-3.24,1.4-3.24,2.58,0,1.61,1.05,2.4,3.42,3.32,2.91,1.12,4.39,2.53,4.39,5.05,0,2.65-1.97,4.95-6.02,4.95-1.66,0-3.47-.48-4.39-1.1l.56-1.86Z\"/>
                            <path class=\"cls-1\" d=\"m16.08,84.85h2.22v8.3h.08c.46-.66.92-1.28,1.35-1.84l5.26-6.46h2.76l-6.23,7.3,6.71,9.9h-2.63l-5.67-8.45-1.63,1.89v6.56h-2.22v-17.2Z\"/>
                            <path class=\"cls-1\" d=\"m34.76,84.85v17.2h-2.22v-17.2h2.22Z\"/>
                            <path class=\"cls-1\" d=\"m41.27,85.06c1.07-.18,2.48-.33,4.26-.33,2.2,0,3.8.51,4.83,1.43.94.82,1.51,2.07,1.51,3.6s-.46,2.78-1.33,3.67c-1.17,1.25-3.09,1.89-5.26,1.89-.66,0-1.28-.02-1.79-.15v6.89h-2.22v-17Zm2.22,8.3c.48.13,1.1.18,1.84.18,2.68,0,4.31-1.3,4.31-3.68s-1.61-3.37-4.06-3.37c-.97,0-1.71.08-2.09.18v6.69Z\"/>
                            <path class=\"cls-1\" d=\"m67.73,86.74h-5.23v-1.89h12.74v1.89h-5.26v15.32h-2.25v-15.32Z\"/>
                            <path class=\"cls-1\" d=\"m82,84.85v7.2h8.32v-7.2h2.25v17.2h-2.25v-8.07h-8.32v8.07h-2.22v-17.2h2.22Z\"/>
                            <path class=\"cls-1\" d=\"m107.96,93.99h-6.69v6.2h7.45v1.86h-9.68v-17.2h9.29v1.86h-7.07v5.44h6.69v1.84Z\"/>
                            <path class=\"cls-1\" d=\"m122.27,84.85h2.22v15.34h7.35v1.86h-9.57v-17.2Z\"/>
                            <path class=\"cls-1\" d=\"m139.17,84.85v17.2h-2.22v-17.2h2.22Z\"/>
                            <path class=\"cls-1\" d=\"m145.67,102.06v-17.2h2.43l5.51,8.7c1.28,2.02,2.27,3.83,3.09,5.59l.05-.03c-.2-2.3-.26-4.39-.26-7.07v-7.2h2.09v17.2h-2.25l-5.46-8.73c-1.2-1.91-2.35-3.88-3.22-5.74l-.08.03c.13,2.17.18,4.24.18,7.1v7.35h-2.09Z\"/>
                            <path class=\"cls-1\" d=\"m174.01,93.99h-6.69v6.2h7.45v1.86h-9.68v-17.2h9.29v1.86h-7.07v5.44h6.69v1.84Z\"/>
                            <path class=\"cls-1\" d=\"m178.65,105.2c.56-1.51,1.25-4.24,1.53-6.1l2.5-.26c-.59,2.17-1.71,5-2.43,6.2l-1.61.15Z\"/>
                            <path class=\"cls-1\" d=\"m210.83,93.28c0,5.92-3.6,9.06-7.99,9.06s-7.73-3.52-7.73-8.73c0-5.46,3.4-9.04,7.99-9.04s7.73,3.6,7.73,8.7Zm-13.35.28c0,3.67,1.99,6.97,5.49,6.97s5.51-3.24,5.51-7.15c0-3.42-1.79-6.99-5.49-6.99s-5.51,3.39-5.51,7.17Z\"/>
                            <path class=\"cls-1\" d=\"m216.34,85.08c1.12-.23,2.73-.36,4.26-.36,2.37,0,3.91.43,4.98,1.4.87.77,1.35,1.94,1.35,3.27,0,2.27-1.43,3.78-3.24,4.39v.08c1.33.46,2.12,1.68,2.53,3.47.56,2.4.97,4.06,1.33,4.72h-2.3c-.28-.48-.66-1.97-1.15-4.11-.51-2.37-1.43-3.27-3.45-3.34h-2.09v7.45h-2.22v-16.98Zm2.22,7.84h2.27c2.37,0,3.88-1.3,3.88-3.27,0-2.22-1.61-3.19-3.96-3.22-1.07,0-1.84.1-2.2.2v6.28Z\"/>
                            <path class=\"cls-1\" d=\"m232.7,85.08c1.35-.2,2.96-.36,4.72-.36,3.19,0,5.46.74,6.97,2.14,1.53,1.4,2.43,3.39,2.43,6.18s-.87,5.11-2.48,6.69c-1.61,1.61-4.26,2.48-7.61,2.48-1.58,0-2.91-.08-4.03-.2v-16.92Zm2.22,15.21c.56.1,1.38.13,2.25.13,4.75,0,7.33-2.65,7.33-7.3.03-4.06-2.27-6.64-6.97-6.64-1.15,0-2.02.1-2.6.23v13.58Z\"/>
                            <path class=\"cls-1\" d=\"m261.24,93.99h-6.69v6.2h7.45v1.86h-9.68v-17.2h9.29v1.86h-7.07v5.44h6.69v1.84Z\"/>
                            <path class=\"cls-1\" d=\"m267.51,85.08c1.12-.23,2.73-.36,4.26-.36,2.37,0,3.91.43,4.98,1.4.87.77,1.35,1.94,1.35,3.27,0,2.27-1.43,3.78-3.24,4.39v.08c1.33.46,2.12,1.68,2.53,3.47.56,2.4.97,4.06,1.33,4.72h-2.3c-.28-.48-.66-1.97-1.15-4.11-.51-2.37-1.43-3.27-3.45-3.34h-2.09v7.45h-2.22v-16.98Zm2.22,7.84h2.27c2.37,0,3.88-1.3,3.88-3.27,0-2.22-1.61-3.19-3.96-3.22-1.07,0-1.84.1-2.19.2v6.28Z\"/>
                            <path class=\"cls-1\" d=\"m306.61,93.28c0,5.92-3.6,9.06-7.99,9.06s-7.73-3.52-7.73-8.73c0-5.46,3.39-9.04,7.99-9.04s7.73,3.6,7.73,8.7Zm-13.35.28c0,3.67,1.99,6.97,5.49,6.97s5.51-3.24,5.51-7.15c0-3.42-1.79-6.99-5.49-6.99s-5.51,3.39-5.51,7.17Z\"/>
                            <path class=\"cls-1\" d=\"m312.12,102.06v-17.2h2.43l5.51,8.7c1.28,2.02,2.27,3.83,3.09,5.59l.05-.03c-.2-2.3-.26-4.39-.26-7.07v-7.2h2.09v17.2h-2.25l-5.46-8.73c-1.2-1.91-2.35-3.88-3.22-5.74l-.08.03c.13,2.17.18,4.24.18,7.1v7.35h-2.09Z\"/>
                            <path class=\"cls-1\" d=\"m331.55,84.85h2.22v15.34h7.35v1.86h-9.57v-17.2Z\"/>
                            <path class=\"cls-1\" d=\"m348.44,84.85v17.2h-2.22v-17.2h2.22Z\"/>
                            <path class=\"cls-1\" d=\"m354.95,102.06v-17.2h2.43l5.51,8.7c1.28,2.02,2.27,3.83,3.09,5.59l.05-.03c-.2-2.3-.26-4.39-.26-7.07v-7.2h2.09v17.2h-2.25l-5.46-8.73c-1.2-1.91-2.35-3.88-3.22-5.74l-.08.03c.13,2.17.18,4.24.18,7.1v7.35h-2.09Z\"/>
                            <path class=\"cls-1\" d=\"m383.28,93.99h-6.69v6.2h7.45v1.86h-9.67v-17.2h9.29v1.86h-7.07v5.44h6.69v1.84Z\"/>
                            </g>
                            <path class=\"cls-2\" d=\"m48.38,7.82c1.34-.99,2.65-1.94,3.94-2.92.11-.09.16-.33.16-.49-.01-.91.5-1.61,1.32-1.75.81-.14,1.52.27,1.8,1.04.29.81-.05,1.54-.85,2.02-.21.13-.4.43-.46.68-.62,2.84-1.21,5.69-1.78,8.54-.09.46-.27.67-.72.76-4.31.87-8.61.87-12.91,0-.41-.08-.61-.27-.69-.71-.57-2.85-1.19-5.7-1.77-8.55-.08-.39-.21-.63-.6-.82-.73-.34-1.01-1.19-.73-1.94.28-.76,1-1.17,1.81-1.02.77.14,1.3.83,1.3,1.64,0,.19,0,.48.12.57,1.3.99,2.63,1.95,3.99,2.95.63-1.43,1.24-2.8,1.82-4.18.07-.16.01-.42-.08-.58-.45-.83-.36-1.64.27-2.18.6-.52,1.48-.51,2.08.01.63.55.71,1.35.25,2.18-.08.15-.15.38-.09.52.59,1.39,1.21,2.78,1.84,4.22Zm4.53-2.43c-1.43,1.05-2.83,2.07-4.21,3.11-.44.33-.7.29-.93-.25-.58-1.38-1.2-2.74-1.79-4.12-.12-.29-.26-.44-.61-.43-.35,0-.54.08-.68.42-.56,1.34-1.15,2.67-1.74,4-.32.74-.43.76-1.07.29-1.37-1.01-2.75-2.02-4.12-3.03-.25.16-.47.29-.7.43,0,.12,0,.23.02.33.45,2.15.91,4.29,1.34,6.44.08.42.27.57.65.64,3.7.71,7.41.8,11.13.24.62-.09,1.41-.05,1.79-.42.39-.38.37-1.19.54-1.81,0-.02.01-.04.02-.06.37-1.78.74-3.56,1.1-5.35-.26-.16-.48-.29-.74-.45Zm-.98,8.51c-4.41.9-8.78.9-13.16,0-.06.79.24,1.08.96,1.21,3.76.66,7.5.64,11.26,0,.76-.13.93-.5.94-1.21Zm-5.7-11.81c-.02-.48-.46-.88-.95-.85-.46.03-.84.45-.83.9.02.49.45.88.94.85.47-.03.84-.44.83-.9Zm-9.63,1.25c-.47,0-.86.38-.88.86-.02.51.39.93.89.92.47-.01.85-.42.84-.9,0-.48-.38-.88-.86-.88Zm18.37.9c0-.51-.39-.92-.88-.9-.47.01-.85.4-.85.87,0,.5.4.92.89.91.46,0,.84-.4.85-.88Z\"/>
                            <g>
                            <path class=\"cls-1\" d=\"m85.98,13.23h-18.31V.61h51.67v12.62h-18.31v53.92h-15.04V13.23Z\"/>
                            <path class=\"cls-1\" d=\"m134.83.61h41.69v12.62h-26.65v13.36h22.69v12.62h-22.69v15.33h27.67v12.62h-42.71V.61Z\"/>
                            <path class=\"cls-1\" d=\"m192.05.61h23.96c14.11,0,25.68,4.9,25.68,20.72s-11.57,21.87-25.68,21.87h-8.92v23.95h-15.04V.61Zm22.74,30.65c7.91,0,12.16-3.39,12.16-9.93s-4.25-8.78-12.16-8.78h-7.7v18.71h7.7Zm-1.53,7.98l10.4-9.63,21.23,37.53h-16.83l-14.81-27.91Z\"/>
                            <path class=\"cls-1\" d=\"m261,.61h15.04v66.54h-15.04V.61Z\"/>
                            <path class=\"cls-1\" d=\"m311.77.61h18l20.9,66.54h-15.92l-8.7-33.7c-1.85-6.65-3.63-14.52-5.4-21.46h-.41c-1.61,7.03-3.39,14.81-5.24,21.46l-8.74,33.7h-15.38L311.77.61Zm-7.12,39h32.02v11.69h-32.02v-11.69Z\"/>
                            <path class=\"cls-1\" d=\"m38.26,21.45h15.04v45.63h-15.04V21.45Z\"/>
                            </g>
                        </g>
                        </g>
                    </svg>
                </a>
            </div>
            <button class=\"navbar-toggler\" type=\"button\" data-bs-toggle=\"collapse\" data-bs-target=\"#navbarNavDropdown\" aria-controls=\"navbarNavDropdown\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
            <span class=\"navbar-toggler-icon\"></span>
          </button>

            <div class=\" collapse navbar-collapse\" id=\"navbarNavDropdown\">
                <ul class=\"navbar-nav ms-auto \">
                    <li class=\"nav-item\">
                        <a class=\"nav-link mx-3 active\" aria-current=\"page\" href=\"menu.php\" >MENU</a>
                    </li>

                    <li class=\"nav-item\">
                        <a class=\"nav-link mx-3 active\" aria-current=\"page\" href=\"menu.php?category=readytoeat\">INSTANT</a>
                    </li>
                    <li class=\"nav-item \">
                        <a class=\"nav-link mx-3 active\" href=\"feedback.php\">REVIEW</a>
                    </li>
                    <li class=\"nav-item dropdown p-0\">
                        <a class=\"nav-link mx-3 data-toggle active shadow p-0 pt-1 my-0\" style=\"width: 30px; border-radius:500px;\" href=\"#\" id=\"navbarDropdownMenuLink\" role=\"button\" data-bs-toggle=\"dropdown\" aria-expanded=\"false\">
                            <img src=\"$imgpath\" alt=\"\" width=\"33\" height=\"33\" class=\"d-inline-block rounded-circle\" >
                        </a>
                        <ul class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\">
                            
                            <li>
                            <form action=\"usersetting.php\" method=\"post\">
                            <button class=\"dropdown-item d-flex justify-content-between\" type=\"submit\">SETTING</button>
                            </form>
                            </li>
                            <li>
                            <form action=\"login.php?exitsession\" method=\"post\">
                            <button class=\"dropdown-item d-flex justify-content-between\" type=\"submit\">LOGOUT</button>
                            </form>
                            </li>

                            
                        </ul>
                    </li>
                    <li class=\"nav-item\">
                        <a class=\"\" href=\"cart.php\"><img src=\"assets/images/cartlogo.png\" alt=\"\" width=\"35\" height=\"35\" class=\"d-inline-block mx-4 mt-1\"></a>
                        
                    </li>
                    <li class=\"nav-item mt-2 ps-3\">
                            <i class=\"fa-regular fa-moon fa-xl\" style=\"color: #000000;\" onclick=\"myFunction()\"></i>
                    </li>
                </ul>

            </div>
        </div>
        </nav>";
        echo $nav;
    }
    function draw_footer(){
        $footer = "<footer class=\"d-block mt-auto\">
        <div class=\"row mx-3  p-3\"></div>
        <div class=\"container d-flex flex-column mt-5 border border-light border-end-0  border-1 border-start-0 w-80 py-4\">

            <div class=\"d-flex flex-row justify-content-around align-items-center\">
                <div class=\"col-md-auto ms-3 d-none d-lg-block d-xl-block d-xxl-block\">
                    <a class=\"navbar-brand\" href=\"index.php\">
                        <img src=\"assets/images/LogoF.PNG\" alt=\"\" width=\"320\" height=\"90\" class=\"d-inline-block\">
                         
                    </a>
                </div>
                <div class=\"col-md-auto ms-3 d-none d-sm-none d-md-none d-lg-none d-xl-block d-xxl-block\">
                    <a class=\"navbar-brand\" href=\"https://www.gcit.edu.bt/\">
                        <img src=\"assets/images/gcit-white.png\" alt=\"\" width=\"126\" height=\"105\" class=\"d-inline-block\">
                        
                    </a>
                </div>
                <div class=\"col-md-auto d-flex flex-column ms-4\">
                    <div class=\"p-2\">
                        <img src=\"assets/images/time.png\" alt=\"\" width=\"17\" height=\"17\" class=\"d-inline-block\">
                        <span class=\"px-3 fw-light\">Monday-Friday: 9:00 am - 6:00 pm, Saturday: 9:00 am - 4:00 pm</span>
                    </div>
                    <div class=\"p-2\">
                        <img src=\"assets/images/email.png\" alt=\"\" width=\"17\" height=\"17\" class=\"d-inline-block\">
                        <span class=\"px-3\">canteen.gcit@rub.edu.bt</span>
                    </div>
                    <div class=\"d-flex flex-row p-2\">
                        <img src=\"assets/images/phone.png\" alt=\"\" width=\"17\" height=\"17\" class=\"d-inline-block\">
                        <span class=\"px-3\">17704392/77304953</span>
                        <img src=\"assets/images/location.png\" alt=\"\" width=\"17\" height=\"17\" class=\"d-inline-block\">
                        <small class=\"px-3\">Gyelpozhing, Bhutan</span>
                    </div>
                    <div class=\"pt-4\">
                        <span class=\"px-3\">Social Media</span>
                        <img src=\"assets/images/facebook.png\" alt=\"\" width=\"20\" height=\"20\" class=\"d-inline-block mx-2\">
                        <img src=\"assets/images/instagram.png\" alt=\"\" width=\"20\" height=\"20\" class=\"d-inline-block mx-2\">
                        <img src=\"assets/images/pintrest.png\" alt=\"\" width=\"20\" height=\"20\" class=\"d-inline-block mx-2\">
                    </div>
                </div>
            </div>
        
        </div>
        <div class=\"bottom container d-flex flex-row justify-content-between  w-100 px-5 mb-3\">
            <div class=\"quick-links px-2 py-3\">
                <a class=\"text-decoration-none text-white px-2\" href=\"aboutus.php\">ABOUT US</a>
            <a class=\"text-decoration-none text-white px-2\" href=\"https://www.gcit.edu.bt/\">GCIT</a>
            <a class=\"text-decoration-none text-white px-2\" href=\"help.php\">HELP</a>
            <a class=\"text-decoration-none text-white px-2\" href=\"privacypolicy.php\">PRIVACY POLICY</a>
            <a class=\"text-decoration-none text-white px-2\" href=\"termsandcondition.php\">TERMS AND CONDITIONS</a>
                    </div>
                    <div class=\"quick-links text-decoration-none text-white px-2 py-3 d-none d-sm-none d-md-none d-lg-block d-xl-block d-xxl-block\">Copyright @ 2022 ITERIA</div>
                </div>
        </footer>";
        echo $footer;
    }
    $darkMode = isset($_COOKIE['dark_mode']) ? ($_COOKIE['dark_mode'] == 'true') : false;
   

    if ($darkMode) {
    echo '<script>document.body.classList.add("dark-mode");</script>';
    }
?>
    <script>
        // function myFunction() {
        // var element = document.body;
        // element.classList.toggle("dark-mode");
        // }

        function myFunction() {
            var element = document.body;
            element.classList.toggle("dark-mode");
            
            var darkMode = element.classList.contains("dark-mode");
            document.cookie = 'dark_mode=' + darkMode;
}
    </script>
<?php
?>
