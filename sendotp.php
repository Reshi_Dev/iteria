<?php
session_start();
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require_once "vendor/autoload.php";
require_once 'connection.php';

$mail = new PHPMailer(true);

//Enable SMTP debugging.
$mail->SMTPDebug = 3;        
$mail->SMTPOptions = array(
    'ssl' => array(
        'verify_peer' => false,
        'verify_peer_name' => false,
        'allow_self_signed' => true
    )
);                       
//Set PHPMailer to use SMTP.
$mail->isSMTP();            
//Set SMTP host name          
// $mail->Host = gethostbyname("smtp.gmail.com");

$mail->Host = "smtp.gmail.com";
//Set this to true if SMTP host requires authentication to send email
$mail->SMTPAuth = true;                          
//Provide username and password    
$mail->Username = "iteria.gcit@gmail.com";                 
$mail->Password = "zrrclppyathtbsgf";                           
//If SMTP requires TLS encryption then set it
$mail->SMTPSecure = "tls";                           
//Set TCP port to connect to

$mail->Port = 587;                                   

$mail->From = "iteria.gcit@gmail.com";
$mail->FromName = "ITERIA";

if(isset($_GET['email'])){
    $email = $_GET['email'];
}
if(isset($_GET['name'])){
    $name = $_GET['name'];
}




$mail->addAddress($email);
$mail->isHTML(true);

$OTP = mt_rand(10000,99999);

$_SESSION['OTP'] = $OTP;


$mail->Subject = "Verify Your E-mail Address";
$mail->Body = "<p>We are happy you signed up for ITERIA</p><br><p> Below is your OTP: </p><b>$OTP</b><p>This is a validation code, not a password. Simply copy this code and paste it into the Account Verification input field.</p><p>Yours truly, <br> ITERIA.</p>";
// $mail->Body = "<p>ok</p>";

try {
    $mail->send();
    header("Location: ../ITERIA/otp.php?otpsent");
    
} catch (Exception $e) {
    // echo "Mailer Error: " . $mail->ErrorInfo;
    header("Location: ../ITERIA/otp.php?otpsentfailed");
}