<?php
    session_start();
    if(!isset($_SESSION['currentuser'])){
        header("Location: ../ITERIA/login.php?loginagain");
        exit();
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>FEEDBACK</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Sarala&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer"
    />


    <style>
        @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@700&family=Sarala&display=swap');
        * {
            font-family: 'Poppins', sans-serif;
            box-sizing: border-box;
        }
        
        .nav-link {
            font-weight: 700;
        }
        
        body {
            height: 100vh;
        }
        
        footer {
            font-weight: 100;
            font-size: 15px;
            color: white;
            background-color: #F6821F
        }
        
        .banner {
            background-image: url('assets/images/banner.png');
            background-size: cover;
            background-repeat: no-repeat;
        }
        /* for page css */
        
        .option {
            background-color: #EEEEEE;
        }
        
        .option a span {
            text-align: center;
        }
        
        .free-coke {
            background-color: #F6821F;
            border-radius: 40px;
        }
        
        .btn-order-now {
            background-color: white;
            color: #F6821F;
        }
        
        .search-icon {
            color: #F6821F;
            background-color: #EEEEEE;
        }
        
        #search-addon {
            position: absolute;
            top: 4px;
            left: 5px;
            z-index: 100;
        }
        @media (min-width: 850px) and (max-width: 1100px) {
            h4{
                font-size: 20px !important;
            }
            p{
                font-size: 14px !important;  
            }
            .card{
                width: 11rem !important;
            }
            .card-btn{
                font-size: 11px !important;
            }
            .item-price, .card-text{
                font-size: 13px !important;
            }
            .card-title{
                font-size: 17px !important;
            }
            .card-img-top{
                width: 150px;
                height: 120px;
            }
            footer {
                font-size: 15px !important;
            }
            /* your code */




        }
        
        @media (max-width: 850px) {
            h4{
                font-size: 18px !important;
            }
            p{
                font-size: 12px !important;  
            }
            .card{
                width: 9rem !important;

            }
            .card-btn{
                font-size: 9px !important;
            }
            .item-price, .card-text{
                font-size: 9px !important;
            }
            .card-title{
                font-size: 11px !important;
            }
            .card-img-top{
                width: 120px;
                height: 90px;
            }

            footer {
                font-size: 13px !important;
            }

            /* your code */
            


        }

        textarea {
            max-width: 100%;
        }
    </style>
</head>

<body class="mt-5 d-flex flex-column min-vh-100">
<?php
        include_once 'clientparts.php';
        $imgpath = $_SESSION['currentuserimgpath'];
        draw_nav_bar($imgpath);
    ?>

    <main>
        <!-- image of banner -->
        <div class="banner mt-4 p-5 w-100">
            <div class="position-relative fs-5 w-100 text-center">FEEDBACK</div>
        </div>
        <!-- The main content -->
        <div class="container main-content-dd w-50 h-100 p-4" >
        <div id="rateYo"></div>
            <div class="feedback-container" >
                <div class="p-5" style="background-color: #F6821F;">
                    <h2 class="text-white fs-5">SEND US YOUR FEEDBACK!</h2>
                    <p class="text-white " style="font-size: 13px;">Do you have a suggestion on our services or found some bug? Let us know in teh field below.</p>
                </div>
                <form action="validatefeedback.php" method="post" id="formfeedback">
                <div class="container p-5 shadow">
                
                    <h5 class="pb-4">HOW WAS YOUR EXPERIENCE?</h5>
                    
                    <input type="hidden"form = "formfeedback" value="" name="date">
                    <input type="hidden" form = "formfeedback" value="43" name="time">
                    <textarea class="p-3" form = "formfeedback"name="userfeedback" cols="45" rows="10" placeholder="Write your feedback"></textarea>
                    <div class="text-center w-100">
                        <button class="btn w-100 text-white btn-block fa-lg gradient-custom-2 p-4 mt-5" name = "sub" type="submit" style="border-radius: 20px; background-color:#F6821F;">SEND FEEDBACK</button>
                    </div>
                </div>
                </form>
            </div>
        </div>

    </main>
    <?php
        include_once 'clientparts.php';
        draw_footer();
    ?>

    
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.js"></script>
    <script>
        $(function () {
 
        $("#rateYo").rateYo({
        rating: 3.6
        });

        });
    </script>
</body>

</html>