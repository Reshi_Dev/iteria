<?php
    session_start();
    if(!isset($_SESSION['currentuser'])){
        header("Location: ../ITERIA/login.php?loginagain");
        exit();
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Sarala&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="index.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer"
    />

    <style>
        @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@700&family=Sarala&display=swap');
        * {
            font-family: 'Poppins', sans-serif;
            box-sizing: border-box;
        }
        
        .nav-link {
            font-weight: 700;
        }
        
        body {
            height: 100vh;
        }
        
        footer {
            font-weight: 100;
            font-size: 15px;
            color: white;
            background-color: #F6821F
        }
        
        .quick-links {
            text-decoration: none;
            color: white;
        }
        
        .banner {
            background-image: url('assets/images/banner.png');
            background-size: cover;
            background-repeat: no-repeat;
        }
        @media (min-width: 850px) and (max-width: 1100px) {
            h4{
                font-size: 20px !important;
            }
            p{
                font-size: 14px !important;  
            }

            footer {
                font-size: 15px !important;
            }
        }
        
        @media (max-width: 850px) {
            h4{
                font-size: 18px !important;
            }
            p{
                font-size: 12px !important;  
            }

            footer {
                font-size: 13px !important;
            }
        }
    </style>
    <title>Document</title>
</head>

<body class="mt-5 d-flex flex-column min-vh-100 ">
<?php
        include_once 'clientparts.php';
        $imgpath = $_SESSION['currentuserimgpath'];
        draw_nav_bar($imgpath);
    ?>

    <main class="">
        <div class=" banner mt-4 p-5 w-100 ">
            <div class="position-relative fs-4 w-100 text-center ">Privacy Policy</div>
        </div>
        <div class="container p-5 w-100">
            <h4>Last Updated on 3rd Febrary,2022</h4>
            <BR></BR>
            <p>This Privacy Policy (“Policy”) describes the policies and procedures on the collection, use, disclosure and protection of your information when you use our website located at swiggy.com, or the Iteria mobile application (collectively, “Iteria
                Platform”) made available by Bundl Technologies Private Limited (“Iteria”, “Company”, “we”, “us” and “our”), a private company established under the laws of India having its registered office at No.55 Sy No.8-14, Ground Floor, I&J Block,
                Embassy Tech Village, Outer Ring Road, Devarbisanahalli, Bengaluru - 560103 </p>
            <p>The terms “you” and “your” refer to the user of the Iteria Platform. The term “Services” refers to any services offered by Iteria whether on the Iteria Platform or otherwise. </p>
            <p>Please read this Policy before using the Iteria Platform or submitting any personal information to Iteria. This Policy is a part of and incorporated within, and is to be read along with, the Terms of Use. </p><br><br>

            <h4>Your Consent</h4>
            <BR></BR>
            <p>By using the Iteria Platform and the Services, you agree and consent to the collection, transfer, use, storage, disclosure and sharing of your information as described and collected by us in accordance with this Policy. If you do not agree
                with the Policy, please do not use or access the Iteria Platform. </p><br><br>


            <h4>POLICY CHANGES</h4>
            <br>
            <p>We may occasionally update this Policy and such changes will be posted on this page. If we make any significant changes to this Policy we will endeavour to provide you with reasonable notice of such changes, such as via prominent notice on
                the Iteria Platform or to your email address on record and where required by applicable law, we will obtain your consent. To the extent permitted under the applicable law, your continued use of our Services after we publish or send a notice
                about our changes to this Policy shall constitute your consent to the updated Policy. </p><br><br>

            <h4>LINK TO OTHER WEBSITE</h4>
            <br>
            <p>The Iteria Platform may contain links to other websites. Any personal information about you collected whilst visiting such websites is not governed by this Policy. Iteria shall not be responsible for and has no control over the practices and
                content of any website accessed using the links contained on the Iteria Platform. This Policy shall not apply to any information you may disclose to any of our service providers/service personnel which we do not require you to disclose
                to us or any of our service providers under this Policy. </p><br><br>
            <h4>INFORMATION WE COLLECT FROM YOU</h4>
            <br>
            <p>Device Information: In order to improve your app experience and lend stability to our services to you, we may collect information or employ third party plugins that collect information about the devices you use to access our Services, including
                the hardware models, operating systems and versions, software, file names and versions, preferred languages, unique device identifiers, advertising identifiers, serial numbers, device motion information, mobile network information, installed
                applications on device and phone state. The information collected thus will be disclosed to or collected directly by these plugins and may be used to improve the content and/or functionality of the services offered to you. Analytics companies
                may use mobile device IDs to track your usage of the Iteria Platform;</p>
        </div>
    </main>
        <?php
        draw_footer();
        ?>
</body>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js " integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p " crossorigin="anonymous "></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js " integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF " crossorigin="anonymous "></script>
</html>