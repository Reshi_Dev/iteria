<?php
    require_once 'connection.php';

    function draw_navbar($type, $args = array()){
        if ($type == "login_bar"){
            echo "
            <nav class='navbar sticky-top jlg_navbar navbar-expand-lg navbar-light' style='background-color: white;'>
            <div class='container-fluid'>
            <a class='navbar-brand big_text light_blue_text' href='#'>JustLetGo</a>
            <button class='navbar-toggler' type='button' data-bs-toggle='collapse' data-bs-target='#navbarText' aria-controls='navbarText' aria-expanded='false' aria-label='Toggle navigation'>
                <span class='navbar-toggler-icon'></span>
            </button>
            <div class='collapse navbar-collapse' id='navbarText'>
                <ul class='navbar-nav me-auto mb-2 mb-lg-0'>
                </ul>
                <ul class='navbar-nav ml-auto mb-2 mb-lg-0'>
                <li class='nav-item'>
                    <a class='nav-link' href='login.php'>Login</a>
                </li>
                <li class='nav-item'>
                    <a class='nav-link' href='signup.php'>Sign Up</a>
                </li>
                </ul>
            </div>
            </div>
            </nav>
            ";
        }
        else if($type == "logout_bar"){
            $uid = "";
            if(isset($_GET['uid'])){
                $uid = $_GET['uid'];
            }

            echo "
            <nav class='navbar sticky-top jlg_navbar navbar-expand-lg navbar-light' style='background-color: white;'>
            <div class='container-fluid'>
            <a class='navbar-brand big_text light_blue_text' href='#'>JustLetGo</a>
            <button class='navbar-toggler' type='button' data-bs-toggle='collapse' data-bs-target='#navbarText' aria-controls='navbarText' aria-expanded='false' aria-label='Toggle navigation'>
                <span class='navbar-toggler-icon'></span>
            </button>
            <div class='collapse navbar-collapse' id='navbarText'>
                <ul class='navbar-nav me-auto mb-2 mb-lg-0'>
                <li class='nav-item'>
                    <a class='nav-link' href='home.php?uid=".$_GET['uid']."'>Home</a>
                </li>
                <li class='nav-item'>
                    <a class='nav-link' href='books.php?uid=".$_GET['uid']."'>Books</a>
                </li>
                <li class='nav-item'>
                    <a class='nav-link' href='musics.php?uid=".$_GET['uid']."'>Musics</a>
                </li>
                <li class='nav-item'>
                    <a class='nav-link' href='test.php?uid=".$_GET['uid']."'>Test</a>
                </li>
                <li class='nav-item'>
                    <a class='nav-link' href='stats.php?uid=".$_GET['uid']."'>Stats</a>
                </li>
                </ul>
                <ul class='navbar-nav ml-auto mb-2 mb-lg-0'>
                <li class='nav-item'>
                    <a class='nav-link' href='index.php'>Log Out</a>
                </li>
                </ul>
            </div>
            </div>
            </nav>
            ";
        }
        else if($type == "goback_bar"){
            echo "
            <nav class='navbar sticky-top jlg_navbar navbar-expand-lg navbar-light' style='background-color: white;'>
            <div class='container-fluid'>
            <a class='navbar-brand big_text light_blue_text' href='#'>JustLetGo</a>
            <button class='navbar-toggler' type='button' data-bs-toggle='collapse' data-bs-target='#navbarText' aria-controls='navbarText' aria-expanded='false' aria-label='Toggle navigation'>
                <span class='navbar-toggler-icon'></span>
            </button>
            <div class='collapse navbar-collapse' id='navbarText'>
                <ul class='navbar-nav me-auto mb-2 mb-lg-0'>
                    <li>
                    </li>
                </ul>
                <ul class='navbar-nav ml-auto mb-2 mb-lg-0'>
                <ul class='navbar-nav me-auto mb-2 mb-lg-0'>
                <li>
                    <div class='click' >
                        <span class='fa fa-star-o'></span>
                        <div class='ring'></div>
                        <div class='ring2'></div>
                        <p class='info'>Added to favorites!</p>
                    </div>
                </li>
                <li class='nav-item'>
                    <a class='nav-link' href='$args[0].php?uid=$args[1]'>Go back</a>
                </li>
                </ul>
            </div>
            </div>
            </nav>
            ";      
        }
    }

    function empty_input_login($email, $password){
        if (empty($email) || empty($password)){
            return true;
        }
        else{
            return false;
        }
    }
    function check_email_user($email){
        global $conn;
        $name_result = $conn->query("select * from users where email='$email'");
        $n = mysqli_num_rows($name_result);
        
        if($n > 0){
            return true;
        }
        else{
            return false;
        }
    }

    function exist_username_email($username_email){
        if(username_taken($username_email) || email_in_use($username_email)){
            return true;
        }
        else{
            return false;
        }
    } 

    function validate_user($email, $password){
        global $conn;
        $sql = "select * from users where email='$email'";
        $result = $conn->query($sql);
        $userdata = $result->fetch_assoc();     
        return password_verify($password, $userdata['password']);
    }

    function get_uid($username_email){
        global $conn;
        $sql = "select uid from users where email='$username_email'";
        $result = $conn->query($sql);
        $userdata = $result->fetch_assoc();

        return $userdata['uid'];
    }

    function empty_input_sign_up($name, $email, $contact, $password, $confirm_password){
        if (empty($name) || empty($email) || empty($contact)  || empty($confirm_password)){
            return true;
        }
        else{
            return false;
        }
    }

    function invalid_name($name){
        if (!preg_match("/^[ a-zA-Z0-9]{5,23}$/", $name)){
            return false;
        }
        else{
            return false;
        }
    }


    function match_password($password1, $password2){
        if ($password1 == $password2){
            return true;
        }
        else{
            return false;
        }
    }

    function invalid_email($email){
        $pattern = "/.gcit@rub.edu.bt/i";
        if(!preg_match($pattern, $email)){
            return true;
        }else{
            return false;
        }
    }

    function email_in_use($email){
        global $conn;
        $email_result = $conn->query("select * from users where email='$email'");
        $n = mysqli_num_rows($email_result);
        
        if($n > 0){
            return true;
        }
        else{
            return false;
        }
    }

    function username_taken($name){
        global $conn;
        $name_result = $conn->query("select * from users where name='$name'");
        $n = mysqli_num_rows($name_result);
        
        if($n > 0){
            return true;
        }
        else{
            return false;
        }
    }

    function create_user($name, $email, $password){
        global $conn;
        $hashed_password = password_hash($password, PASSWORD_DEFAULT);
        $sql = "insert into users (name, email, password, favorite_books, favorite_musics, book_activities, music_activities) values ('$name', '$email', '$hashed_password', '[]', '[]', '{}', '{}')";
        if($conn->query($sql)){
            return true;
        }
        else{
            return false;
        }
    }

    function get_view_activities($book_id){
        global $conn;

        $result = $conn->query("select view_activities from books where id = '$book_id'");
        $row = $result->fetch_assoc();
        $view_activities = $row['view_activities'];

        return $view_activities;
    }

    function get_stream_activities($music_id){
        global $conn;

        $result = $conn->query("select stream_activities from musics where id = '$music_id'");
        $row = $result->fetch_assoc();
        $stream_activities = $row['stream_activities'];

        return $stream_activities;
    }

    function is_conduct_test_taken($uid, $ymd){
        global $conn;

        $result = $conn->query("select test_result from users where id='$uid'");
        $row = $result->fetch_assoc();
        $string_array = $row['test_result'];
        $test_result = json_decode($string_array, true);
    
        return array_key_exists($ymd, $test_result);
    }

    function get_total_conduct_test($uid){
        global $conn;

        $result = $conn->query("select test_result from users where id='$uid'");
        $row = $result->fetch_assoc();
        $string_array = $row['test_result'];
        $test_result = json_decode($string_array, true);
    
        return count($test_result);
    }

    function has_words($string, $words){
        $count = 0;
        echo var_dump($words) . "<br>";
        foreach($words as $word){
            if (str_contains($string, $word)){
                echo $word . "<br>";
                $count += 1;
            }
        }

        return $count;
    }