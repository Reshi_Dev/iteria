<?php
    session_start();
    if(isset($_POST['Name'])){
        $name = $_POST['Name'];
        $email = $_POST['Email'];
        $contactNo = $_POST['ContactNo'];
        $pwd = $_POST['Password'];
        $confirm_password = $_POST['ConfirmPassword'];

        
        require_once 'connection.php';
        require_once 'functions.inc.php';
 
        if (empty_input_sign_up($name, $email, $contactNo, $password, $confirm_password)){
            header("location: ../ITERIA/signup.php?error=emptyinput");
            exit();
        }
        else if(invalid_name($name)){
            header("location: ../ITERIA/signup.php?error=invalidname");
            exit();
        }
        else if(invalid_email($email)){
            header("location: ../ITERIA/signup.php?error=invalidemail");
            exit();
        }
        else if(!match_password($pwd, $confirm_password)){
            header("location: ../ITERIA/signup.php?error=passworddonotmatch");
            exit();
        }   
        else if(email_in_use($email)){
            header("location: ../ITERIA/signup.php?error=emailinuse");
            exit();
        }
        else{
            // $_SESSION['regemail'] = $email;
            $hashed_pwd = password_hash($pwd, PASSWORD_DEFAULT);
            $_SESSION['regpwd'] = $hashed_pwd;
            $_SESSION['regname'] = $name;
            $_SESSION['regemail'] = $email;
            $_SESSION['regcontactno'] = $contactNo;

            header("location: ../ITERIA/sendotp.php?email=$email&name=$name");
            exit();
            
            
        
        }
    }else{
        echo "failed";
    }
?>