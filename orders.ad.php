<?php
    session_start();
    if(!isset($_SESSION['currentuser'])){
        header("Location: ../ITERIA/login.php?loginagain");
        exit();
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@700&display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer"
    />
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Sarala&display=swap" rel="stylesheet">

    <title>Document</title>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@700&family=Sarala&display=swap');
        * {
            box-sizing: border-box;
            margin: 0;
            padding: 0;
            font-family: 'Poppins', sans-serif;
        }
        
        #main::-webkit-scrollbar {
            display: none;
        }
        
        .side-bar-text {
            font-size: .9rem;
        }
        /* for sliding btn */

        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }
        
        .switch input {
            opacity: 0;
            width: 0;
            height: 0;
        }
        
        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }
        
        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }
        
        input:checked+.slider {
            background-color: #F6821F;
        }
        
        input:checked+.slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }
        
        .slider.round {
            border-radius: 34px;
        }
        
        .slider.round:before {
            border-radius: 50%;
        }
        td {
            padding: 20px;
        }

        
        @media (min-width: 850px) and (max-width: 1100px) {
            .feedback-text {
                font-size: 14px !important;
            }
            .nav-header {
                font-size: 19px !important;
            }
        }
        
        @media (max-width: 850px) {
            .feedback-text {
                font-size: 11px !important;
            }
            .nav-header {
                font-size: 17px !important;
            }
        }
    </style>
</head>

<body class="">
    <main class="w-100" style="height: 50px;">
        <!-- main parent -->
        <div class="d-flex flex-row w-100" style="height:100%;">
            <!-- left part -->
            <?php
                include_once 'component.ad.php';
                draw_side_bar();
            ?>
            <!-- right part -->
            <section class="righta w-100">
            <?php
                draw_nav("ORDERS");
            ?>
                <section class=" main-content border-dark position-absolute bottom-0 " id="main" style=" padding: 60px;height:100vh;overflow:scroll; height: 533px;">
                    <!-- your code here! -->
                    <div class="container">
                        <form action="orders.ad.php" method="post">
                        <div class="date my-5">
                            <h1 class="d-inline-block fs-4 me-3">Date </h1> 
                            <input type="date" name="date" class="me-3" >
                            <input type="submit" name="subdate" class="btn text-white" style ="background-color:  #F6821F">
                        </div>
                        </form>
                        <div class="container">
                                <?php
                                    include_once 'connection.php';
                                    include_once 'component.ad.php';
                                ?>
                                <form action="orders.ad.php" method="post">
                            <button class="btn text-white w-100" name="subb" type="submit" style ="background-color:  #F6821F">Serve</button>
                        </form>
                            <table class="table table-light table-bordered ">
                                <thead>
                                    <tr class="">
                                        <th scope="col">Order ID</th>
                                        <th scope="col">Date</th>
                                        <th scope="col">Time</th>
                                        <th scope="col">Location</th>
                                        <th scope="col">Contact no</th>
                                        <th scope="col">Items</th>
                                        <th scope="col">Qty</th>
                                    </tr>
                                </thead>
                                ​
                                <tbody>
                                <?php

                                    $item_price = array();
                                    if(isset($_POST['subb'])){
                                        
                                        $quu = "SELECT totalprice from orders order by date LIMIT 1;";
                                        $rsl = $conn->query($quu);
                                        $userd = $rsl->fetch_assoc(); 
                                        if(isset($userd['totalprice'])){
                                            $first_total = $userd['totalprice'];

                                            $rev = $first_total;
                                            $sql = "select * from admin;";
                                            $result = $conn->query($sql);
                                            $userdata = $result->fetch_assoc();  
                                            $old_rev = $userdata['revenue'];
                                            $old_exp = $userdata['expenses'];

                                            $new_rev = $old_rev + $rev;
                                            $gross = $new_rev - $old_exp;

                                            $que1 = "UPDATE admin set revenue = $new_rev, grossprofit = $gross;";
                                            mysqli_query($conn, $que1);

                                            $queryy = "DELETE from orders ORDER by date LIMIT 1;";
                                            $z = mysqli_query($conn, $queryy);
                                        }else{
                                            echo "No orders are available!";
                                        }
                                        
                                        
                                    
                                    }
                                    
              
                                    if(isset($_POST['subdate'])){
                                        $sort_date = $_POST['date'];
                                        $sql = "SELECT * from orders where date = '$sort_date';";
                                        $result = $conn->query($sql);
                                        if(mysqli_num_rows($result) > 0){    
                                            while ($row = mysqli_fetch_assoc($result)){
                                                $order = json_decode($row['orderitems'], true);
                                                $items = array();
                                                $qty = array();
                                                
                                                foreach ($order as $a){
                                                    $id = $a['itemID'];
                                                    $innersql = "SELECT itemname from items where itemid = $id;";
                                                    $res = $conn->query($innersql);
                                                    $z =  mysqli_fetch_assoc($res);
                                                    array_push($items, $z['itemname']);
                                                }
                                                foreach ($order as $a){
                                                    $id = $a['itemID'];
                                                    $innersql = "SELECT itemprice from items where itemid = $id;";
                                                    $res = $conn->query($innersql);
                                                    $z =  mysqli_fetch_assoc($res);
                                                    $fina_qty = $a['qty']/$z['itemprice'];
                                                    array_push($qty, $fina_qty);
                                                }
                                                $user_id = $row['uid'];
                                               $sql_query = "SELECT * from users where uid = $user_id;";
                                               $res = $conn->query($sql_query);
                                               $z =  mysqli_fetch_assoc($res);
                                                
        
                                                draw_orders($row['orderid'], $row['date'], $row['time'], $row['location'], $items, $qty, (int)count($items), $z['contactno']);
                                                
                                            }
                                        }else{
                                            echo  '<div class = "text-dark fs-5 ps-4 text-normal" > <br> No orders for date  '.$sort_date.' <span class = "fs-2"></span><div/>';
                                        }
                                    }else{
                                        $sql = "SELECT * from orders ORDER by date;";
                                        $result = $conn->query($sql);
                                        if(mysqli_num_rows($result) > 0){  
                                            $counter = 0;  
                                            while ($row = mysqli_fetch_assoc($result)){
                                                $order = json_decode($row['orderitems'], true);
                                                $items = array();
                                                $qty = array();
                                                
                                                foreach ($order as $a){
                                                    $id = $a['itemID'];
                                                    $innersql = "SELECT itemname from items where itemid = $id;";
                                                    $res = $conn->query($innersql);
                                                    $z =  mysqli_fetch_assoc($res);
                                                    array_push($items, $z['itemname']);
                                                }
                                                foreach ($order as $a){
                                                    $id = $a['itemID'];
                                                    $innersql = "SELECT itemprice from items where itemid = $id;";
                                                    $res = $conn->query($innersql);
                                                    $z =  mysqli_fetch_assoc($res);
                                                    // echo $z['itemprice'];
                                                    // echo "<br>";
                                                    $fina_qty = $a['qty']/$z['itemprice'];
                                                    array_push($qty, $fina_qty);
                                                    
                                                }
                                                
                                                if($counter == 0){
                                                    foreach ($order as $a){
                                                        array_push($item_price, $a['qty']) ;
                                                        $counter += 1;
                                                    }
                                                }
                                               $user_id = $row['uid'];
                                            //    echo $user_id;
                                               $sql_query = "SELECT * from users where uid = '$user_id';";
                                               $res = $conn->query($sql_query);
                                               $z =  mysqli_fetch_assoc($res);
                                            //    echo $z['name'];
                                                

                                                draw_orders($row['orderid'], $row['date'], $row['time'], $row['location'], $items, $qty, (int)count($items), $z['contactno']);
                                                
                                            }
                                        }else{
                                            echo  '<div class = "text-dark fs-5 ps-4 text-normal" > <br> NO ORDERS! JOB WELL DONE <span class = "fs-2"></span> <div/>';
                                        }
                                    }
                                    ?>
                                    
                                </tbody>
                            </table>
                        </div>
                        
                        
                    </div>


                    ​
                </section>
                ​
            </section>
        </div>
        ​
    </main>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script src="graph.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    ​
</body>
​

</html>
