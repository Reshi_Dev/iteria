<?php
    session_start();
    if(!isset($_SESSION['currentuser'])){
        header("Location: ../ITERIA/login.php?loginagain");
        exit();
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MENU</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Sarala&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer"
    />


    <style>
        @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@700&family=Sarala&display=swap');
        * {
            font-family: 'Poppins', sans-serif;
            box-sizing: border-box;
        }
        
        .nav-link {
            font-weight: 700;
        }
        
        body {
            height: 100vh;
        }
        
        footer {
            font-weight: 100;
            font-size: 15px;
            color: white;
            background-color: #F6821F
        }
        
        .banner {
            background-image: url('assets/images/banner.png');
            background-size: cover;
            background-repeat: no-repeat;
        }
        /* for page css */
        
        .option {
            background-color: #EEEEEE;
        }
        
        .option a span {
            text-align: center;
        }
        
        .free-coke {
            background-color: #F6821F;
            border-radius: 40px;
        }
        
        .btn-order-now {
            background-color: white;
            color: #F6821F;
        }
        
        .search-icon {
            color: #F6821F;
            background-color: #EEEEEE;
        }
        
        #search-addon {
            position: absolute;
            top: 4px;
            left: 5px;
            z-index: 100;
        }
        @media (min-width: 850px) and (max-width: 1100px) {
            h4{
                font-size: 20px !important;
            }
            p{
                font-size: 14px !important;  
            }

            footer {
                font-size: 15px !important;
            }
        }
        
        @media (max-width: 850px) {
            h4{
                font-size: 18px !important;
            }
            p{
                font-size: 12px !important;  
            }

            footer {
                font-size: 13px !important;
            }
        }
    </style>
</head>

<body class="mt-5 d-flex flex-column min-vh-100">
<?php
        include_once 'clientparts.php';
        $imgpath = $_SESSION['currentuserimgpath'];
        draw_nav_bar($imgpath);
    ?>
    <main class="">
        <div class="container m-5">
            <h4>ITERIA-Term Of Use</h4>
            <p>This document is an electronic record in terms of Information Technology Act, 2000 and rules there under as applicable and the amended provisions pertaining to electronic records in various statutes as amended by the Information Technology
                Act, 2000. This document is published in accordance with the provisions of Rule 3 (1) of the Information Technology (Intermediaries guidelines) Rules, 2011 that require publishing the rules and regulations, privacy policy and Terms of
                Use for access or usage of www.iteria.com website and Iteria application for mobile and handheld devices. </p><br><br>
            <h5>Term Of Use</h5>
            <p>These terms of use (the "Terms of Use") govern your use of our website www.iteria.com (the "Website") and our "Iteria" application for mobile and handheld devices (the "App"). The Website and the App are jointly referred to as the "Platform".
                Please read these Terms of Use carefully before you use the services. If you do not agree to these Terms of Use, you may not use the services on the Platform, and we request you to uninstall the App. By installing, downloading or even
                merely using the Platform, you shall be contracting with Iteria and you signify your acceptance to this Terms of Use and other Iteria policies (including but not limited to the Cancellation & Refund Policy, Privacy Policy and Take Down
                Policy) as posted on the Platform and amended from time to time, which takes effect on the date on which you download, install or use the Platform, and create a legally binding arrangement to abide by the same. </p><br><br>
            <p>Iteria enables transactions on its Platform between participating restaurants/merchants and buyers, dealing in (a) prepared food and beverages, (b) consumer goods, and (c) other products and services ("Platform Services"). The buyers ("Buyer/s")
                can choose and place orders ("Orders") from a variety of products and services listed and offered for sale by various merchants including but not limited to the restaurants, eateries and grocery stores ("Merchant/s"), on the Platform.
                Further, the Buyer can also place Orders for undertaking certain tasks on the Platform (“Tasks”). </p><br><br>
            <p>Iteria enables delivery of such Orders or completion of Tasks at select localities of serviceable cities across GCIT campus ("Delivery Services") by connecting third party service providers i.e. pick-up and delivery partners (“PDP”) who will
                be responsible for providing the pick-up and delivery services and completing Tasks initiated by the users of the Platform (Buyers or Merchants). The Platform Services and Delivery Services are collectively referred to as "Services". For
                both Platform Services and Delivery Services, Iteria is merely acting as an intermediary between the Merchants and Buyers and/or PDPs and Buyers/Merchants. </p><br><br>
            <p>PDPs are individual entrepreneurs engaged with Iteria on a voluntary, non-exclusive and principal to principal basis to p rovide aforementioned services for service fee. PDPs are independent contractors and are free to determine their timings
                of work. Iteria does not exercise control on the PDPs and the relationship between the PDPs and Iteria is not that of an agent and principal or employee and employer. </p><br><br>
            <p> For the pickup and delivery services and completing the Tasks, PDPs may charge the users of the Platform (Buyers or Merchants), a service fee (inclusive of applicable taxes whenever not expressly mentioned) determined on the basis of various
                factors including but not limited to distance covered, time taken, demand for delivery services/Tasks, real time analysis of traffic and weather conditions, seasonal peaks or such other parameters as may be determined from time to time.
            </p>
        </div>
    </main>

    <?php
        include_once 'clientparts.php';
        draw_footer();
    ?>



    
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
</body>

</html>