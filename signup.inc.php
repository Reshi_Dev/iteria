<?php
    if(isset($_POST['submit'])){
        $name = $_POST['name'];
        $email = $_POST['email'];
        $password = $_POST['password'];
        $confirm_password = $_POST['confirm_password'];

        require_once 'dbh.inc.php';
        require_once 'functions.inc.php';

        if (empty_input_sign_up($name, $email, $password, $confirm_password)){
            header("location: ../signup.php?error=emptyinput");
            exit();
        }
        else if(invalid_name($name)){
            header("location: ../signup.php?error=invalidname");
            exit();
        }
        else if(invalid_email($email)){
            header("location: ../signup.php?error=invalidemail");
            exit();
        }
        else if(invalid_password($password)){
            header("location: ../signup.php?error=invalidpassword");
            exit();
        }
        else if(!match_password($password, $confirm_password)){
            header("location: ../signup.php?error=passworddonotmatch");
            exit();
        }
        else if(username_taken($name)){
            header("location: ../signup.php?error=usernameistaken");
            exit();
        }
        else if(email_in_use($email)){
            header("location: ../signup.php?error=emailinuse");
            exit();
        }
        else if(!create_user($name, $email, $password)){
            header("location: ../signup.php?error=signupfailed");
            exit();
        }
        else{
            header("location: ../signup.php?success");
            exit();
        }
    }
?>