<?php
    session_start();
    if(!isset($_SESSION['currentuser'])){
        header("Location: ../ITERIA/login.php?loginagain");
        exit();
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Sarala&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="index.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer"
    />

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@700&family=Sarala&display=swap');
        * {
            font-family: 'Poppins', sans-serif;
            box-sizing: border-box;
        }
        
        .nav-link {
            font-weight: 700;
        }
        
        body {
            height: 100vh;
        }
        
        footer {
            font-weight: 100;
            font-size: 15px;
            color: white;
            background-color: #F6821F
        }
        
        .quick-links {
            text-decoration: none;
            color: white;
        }
        
        .banner {
            background-image: url('assets/images/banner.png');
            background-size: cover;
            background-repeat: no-repeat;
        }
    </style>
</head>

<body class="mt-5 d-flex flex-column min-vh-100">
<?php
        include_once 'clientparts.php';
        $imgpath = $_SESSION['currentuserimgpath'];
        draw_nav_bar($imgpath);
    ?>
    <main class="w-100" style="width: 100%;">
        <div class="banner mt-4 p-5 w-100">
            <div class="position-relative fs-4 w-100 text-center">HELP</div>
        </div>
        <div class="ms-5 ps-5 container p-4 m-4 main-content-dd w-100 h-100 d-felx justify-content-center align-items-center">
            <div class="row">
                <div class="col text-center">
                    <h2>How to order?</h2>
                </div>

            </div>
            <div class="row w-100 mx-5 my-5">
                <div class="col">SEE WHAT IS AVAILABLE</div>
                <div class="col">
                    <img src="assets/images/img4.png" alt="" width="80px">
                </div>
                <div class="col">MAKE YOUR ORDER</div>
                <div class="col">
                    <img src="assets/images/img2.png" alt="" width="80px">
                </div>
            </div>
            <div class="d-flex justify-content-between w-100 mx-5 my-5">
                <div class="col"><img src="assets/images/img3.png" alt="" width="80px"></div>
                <div class="col">ADD ITMES IN YOUR CART</div>
                <div class="col"><img src="assets/images/img1.png" alt="" width="80px"></div>
                <div class="col">FOOD IS ON THE WAY</div>
            </div>
        </div>
    </main>


    <?php
        draw_footer();
    ?>
</body>
</html>