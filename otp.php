<?php
    session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Sarala&display=swap" rel="stylesheet">
</head>
<body>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@700&family=Sarala&display=swap');

        body{
            font-family: 'Poppins', sans-serif;
            background: #ffff;
        }
        .height-100 {
            height: 100vh
        }

        .card {
            width: 400px;
            border: none;
            height: 300px;
            box-shadow: 0px 5px 20px 0px #d2dae3;
            z-index: 1;
            display: flex;
            justify-content: center;
            align-items: center
        }

        .card h6 {
            color: #F6821F;
            font-size: 20px
        }

        .inputs input {
            width: 40px;
            height: 40px
        }

        input[type=number]::-webkit-inner-spin-button,
        input[type=number]::-webkit-outer-spin-button {
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            margin: 0
        }

        .card-2 {
            background-color: #fff;
            padding: 10px;
            width: 350px;
            height: 100px;
            bottom: -50px;
            left: 20px;
            position: absolute;
            border-radius: 5px
        }

        .card-2 .content {
            margin-top: 50px
        }

        .card-2 .content a {
            color: #F6821F
        }

        .form-control:focus {
            box-shadow: none;
            border: 2px solid #F6821F
        }

        .validate {
            border-radius: 20px;
            height: 40px;
            background-color: #F6821F;
            border: 1px solid #F6821F;
            width: 140px
        }
        
    </style>
<div class="container height-100 d-flex justify-content-center align-items-center">
    <div class="position-relative">
        <div class="card p-5 text-center" style="">
            
            <h4 class = "">Please enter the OTP <br> to verify your account</h4 >
            <br>    
                
            <div>
                 
                 <?php 
                if(isset($_GET['otpsent'])){
                    echo "<span class=\"text-success\">A code has been sent to your email</span>";
                    // echo $_SESSION['OTP'];
                }else if(isset($_GET['otpsentfailed'])){
                    echo "<span>OTP sent failed! Please register again </span> ";
                }else if (isset($_GET['wrongotp'])){
                    echo "<span class=\"text-danger\">Your OTP does not match. </span>";
                }
            ?>    
            </div>
            <br>
            <form action="validateotp.php" method="post">
                <div id="otp" class="inputs d-flex flex-row justify-content-center mt-2"> 
                    <input class="m-2 text-center form-control rounded" name="otp1" type="text" id="first" maxlength="1" /> 
                    <input class="m-2 text-center form-control rounded" name="otp2" type="text" id="second" maxlength="1" />
                    <input class="m-2 text-center form-control rounded" name="otp3" type="text" id="third" maxlength="1" />
                    <input class="m-2 text-center form-control rounded" name="otp4" type="text" id="fourth" maxlength="1" /> 
                    <input class="m-2 text-center form-control rounded" name="otp5" type="text" id="fifth" maxlength="1" /> 
                    <!-- <input class="m-2 text-center form-control rounded" name="otp" type="text" id="sixth" maxlength="1" />  -->
                </div>
            <div class="mt-4"> <button class="btn btn-success px-4 validate" type="submit" name="submit">Validate</button> </div>
            </form>
            
        </div>
    </div>
</div>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
<script>
    document.addEventListener("DOMContentLoaded", function(event) {

    function OTPInput() {
        const inputs = document.querySelectorAll('#otp > *[id]');
    for (let i = 0; i < inputs.length; i++) { inputs[i].addEventListener('keydown', function(event) { if (event.key==="Backspace" ) { inputs[i].value='' ; if (i !==0) inputs[i - 1].focus(); } else { if (i===inputs.length - 1 && inputs[i].value !=='' ) { return true; } else if (event.keyCode> 47 && event.keyCode < 58) { inputs[i].value=event.key; if (i !==inputs.length - 1) inputs[i + 1].focus(); event.preventDefault(); } else if (event.keyCode> 64 && event.keyCode < 91) { inputs[i].value=String.fromCharCode(event.keyCode); if (i !==inputs.length - 1) inputs[i + 1].focus(); event.preventDefault(); } } }); } } OTPInput(); });
</script>
</body>
</html>